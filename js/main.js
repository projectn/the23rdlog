var screenWidth = function(){
    if(window.navigator.platform == "MacIntel"){
        return window.innerWidth;
    } else {
        return screen.width;
    }
}
var screenHeight = function(){
    if(window.navigator.platform == "MacIntel"){
        return window.innerHeight;
    } else {
        return screen.height;
    }
}
cameras = [];
function addcamera(camera){
    cameras.push(camera);
}
var blankscene = new THREE.Scene();
blankscene.background = new THREE.Color(0xffffff);
var clock = new THREE.Clock();
var sound = true;
var limit = 15000;
fadeinterval = null;
fov = 60;
sensitivity = 6;
var blankcamera = new THREE.PerspectiveCamera(fov, screenWidth()/screenHeight(), 0.1, 100000);
addcamera(blankcamera);
var renderer = new THREE.WebGLRenderer({antialias:true});
renderer.setSize(screenWidth(),screenHeight());
document.body.appendChild(renderer.domElement);
var fade = document.createElement('div');
fade.className = "fade";
fade.id = "fade";
fade.style.opacity = 0;
document.body.appendChild(fade);
var blank = function(delta){
	renderer.render(blankscene,blankcamera);
}
var blankload = function(){

}
var blankunload = function(){
    
}
var blankpause = function(){
	
}
var blankunpause = function(){
	
}
var levelname = "blank";
newlevel = "";
vox = new Audio("media/bensound-slowmotion.ogg");
vox.volume = 0;
musicloop = new Audio("media/bensound-slowmotion.ogg");
musicloop.loop = true;
var render = function(){
	setTimeout( function() {

        requestAnimationFrame( render );

    }, 1000 / 30 );
	//delta = clock.getDelta();
    delta = 1/30; //Fake Delta
	window[levelname](delta);
	
}
function fadeout(){
    fade = document.getElementById("fade");
    if(parseFloat(fade.style.opacity) < 1 && musicloop.volume > 0){
    musicloop.volume -= 0.03999;
    fade.style.opacity=parseFloat(fade.style.opacity)+0.04;
    }
    else{
        fade.style.opacity = 1;
        musicloop.pause();
        musicloop.volume = 0;
        clearInterval(fadeinterval);
        window[levelname+"unload"]();
        unloadDone();
    }
}
function fadein(){
    if(parseFloat(fade.style.opacity) > 0 && musicloop.volume < 1){
        musicloop.volume += 0.01999;
        fade.style.opacity= parseFloat(fade.style.opacity) - 0.02;
    }
    else{
        fade.style.opacity = 0;
        musicloop.volume = 1;
        clearInterval(fadeinterval);
    }
}
function switchLevel(newlevel){
    window.newlevel = newlevel;
    if(fadeinterval != null){
    clearInterval(fadeinterval);
    }
    fadeinterval = window.setInterval(fadeout,20);
}
function unloadDone(){
    levelname = newlevel;
	window[levelname+"load"]();
    loadDone();
}
function loadDone(){
    musicloop.volume = 0;
    fadeinterval = window.setInterval(fadein,1);
}
function loadScript(src,callback){
	var script = document.createElement('script');
	script.src = src;
	script.onload = callback;
	document.body.appendChild(script);
}
render();