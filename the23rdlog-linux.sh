#!/bin/bash
cd /home/trevor/the23rdlog/
pkill chromium
chromium-browser --kiosk "http://localhost:8080/test.html" --disable-infobars --incognito --disable-background-networking &
ruby serve.rb -Rconfig.ru -p8080 -a127.0.0.1 start &
#python serve.py &
python poll.py
pkill chromium
#killall ruby
killall python
