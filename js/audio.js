function playsong(url){
	var audio = new Audio(url);
	audio.play();
	return audio;
}
function loopsong(url){
	var audio = new Audio(url);
	audio.play();
	audio.addEventListener('ended', function() {this.currentTime = 0;this.play();}, false);
	return audio;
}
function looptwo(url){
	var audio = new Audio(url);
	audio.play();
	audio.loop = true;
	return audio;
}
function playsongcallback(url,callback){
	var audio = new Audio(url);
	audio.play();
	audio.addEventListener('ended', callback, false);
	return audio;
}