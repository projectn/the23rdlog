function checkWebGL(){
	try{
	var canvas = document.createElement('canvas'); return !! (window.WebGLRenderingContext && (canvas.getContext('webgl') || canvas.getContext('experimental-webgl')));
	}catch(e){
	return false;
	}
}
function checkPointerLock(){
	var lock = 'pointerLockElement' in document || 'mozPointerLockElement' in document || 'webkitPointerLockElement' in document;
	if(lock){
		return true;
	}
	return false;
}