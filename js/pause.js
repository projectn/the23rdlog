function createPauseMenu(){
    pausemenu = document.createElement("div");
    pausemenu.className = "menu";
    pausemenu.id = "pausemenu";
    pausemenu.innerHTML = "Paused<br /><br /><a onclick='pointerlock();'>Play</a><br /><a onclick='settings();'>Settings</a><br /><a onclick='switchLevel(\"mainmenu\");'>Exit</a>"
    document.body.appendChild(pausemenu);
    settingsmenu = document.createElement("div");
    settingsmenu.className = "menu";
    settingsmenu.id = "settingsmenu";
    settingsmenu.innerHTML = "Paused<br /><br />FOV:<a onclick='setfov(60);'>60</a>&nbsp;<a onclick='setfov(75);'>75</a>&nbsp;<a onclick='setfov(90);'>90</a><br />Sensitivity:<a onclick='setsensitivity(9);'>0.6</a>&nbsp;<a onclick='setsensitivity(7.5);'>0.8</a>&nbsp;<a onclick='setsensitivity(6);'>1.0</a><br />Sound:<a onclick='setsound(false);'>Off</a>&nbsp;<a onclick='setsound(true);'>On</a><br /><a onclick='unsettings();'>Back</a>"
    document.body.appendChild(settingsmenu);
    document.getElementById("pausemenu").style.display = "none";
    document.getElementById("settingsmenu").style.display = "none";
}
function setfov(value){
    fov = value;
    for(var i = 0; i < cameras.length; i++){
        cameras[i].fov = value;
        cameras[i].updateProjectionMatrix();
    }
}
function setsensitivity(value){
    sensitivity = value;
}
function setsound(value){
    sound = value;
}

function settings(){
    document.getElementById("pausemenu").style.display = "none";
    document.getElementById("settingsmenu").style.display = "block";
}
function unsettings(){
    document.getElementById("pausemenu").style.display = "block";
    document.getElementById("settingsmenu").style.display = "none";
}
function pause(){
    document.getElementById("pausemenu").style.display = "block";
    musicloop.pause();
    vox.pause();
    engineloop.pause();
}
function unpause(){
    document.getElementById("pausemenu").style.display = "none";
    musicloop.play();
    engineloop.play();
    if(!vox.ended){
        vox.play();
    }
}
function destroyPauseMenu(){
    document.getElementById("settingsmenu").parentNode.removeChild(document.getElementById("settingsmenu"));
    document.getElementById("pausemenu").parentNode.removeChild(document.getElementById("pausemenu"));
}