var leveltwo = function (delta) {
    /*if (scenecontainer.position.x > limit) {
        scenecontainer.position.x = limit;
    } else if (scenecontainer.position.x < (limit * -1)) {
        scenecontainer.position.x = (limit * -1);
    }
    if (scenecontainer.position.y > limit) {
        scenecontainer.position.y = limit;
    } else if (scenecontainer.position.y < (limit * -1)) {
        scenecontainer.position.y = (limit * -1);
    }
    if (scenecontainer.position.z > limit) {
        scenecontainer.position.z = limit;
    } else if (scenecontainer.position.z < (limit * -1)) {
        scenecontainer.position.z = (limit * -1);
    }*/
    if (sound) {
        musicloop.volume = 0.5;
    } else {
        musicloop.volume = 0;
    }
    if (pointerlocked == true) {
        if(currentpart == 0){
            vox = playsong("media/espionage.ogg");
            //musicloop = loopsong("media/overture.ogg");
            currentpart = 1;
        }
        if(currentpart == 1){
            if(center.getWorldPosition().length() < 1000){
                vox = playsong("media/retrieve.ogg");
                currentpart = 2;
            }
        }
        if(currentpart == 2){
            if(center.getWorldPosition().length() > 7000){
                vox = playsongcallback("media/goodjob.ogg",function(){currentpart = 4;});
                currentpart = 3;
            }
        }
        if(currentpart == 4){
            switchLevel("levelthree");
        }
        if(typeof cone != undefined){
        updateConeTwo();
        levelcheck();
        }
        updateControls(delta);
        updateShip(delta);
        updateBullets(delta);
        renderer.render(leveltwoscene,leveltwocamera);
    }
}
function updateConeTwo(){
    var myDirectionVector = center.getWorldPosition().clone();
    var mx = new THREE.Matrix4().lookAt(myDirectionVector,new THREE.Vector3(0,0,0),new THREE.Vector3(0,1,0));
    var qt = new THREE.Quaternion().setFromRotationMatrix(mx);
    //mesh.rotation.x += 0.01;
    //mesh.rotation.y += 0.02;
    
    
    cone.quaternion.copy( qt );
    cone.position.setFromMatrixPosition(conedummy.matrixWorld);
    //cone.lookAt(center.getWorldPosition());
}
function levelcheck(){
    for(var i = 0; i < spheresss.length; i++){
        if(spheresss[i].getWorldPosition().length() < 500){
            oldlevel = "leveltwo";
            switchLevel("fail");
        }
    }
}
function levelparse(text){
    vectors = [];
    newlines = text.split("\n");
    for(var i = 0; i < newlines.length; i++){
texts = newlines[i].split(/,|\\\(/);
if(texts.length == 3){
x = parseInt(texts[0].replace('(',''));
y = parseInt(texts[1]);
z = parseInt(texts[2].replace(')',''));
vectors.push(new THREE.Vector3(x,y,z));
}
}

}
function detectors(){
    spheresss = [];
    var geometry = new THREE.IcosahedronGeometry( 500, 1 );
var material = new THREE.MeshStandardMaterial( {color:0xffffff,wireframe:true,emissive:0x002200} );
    for(var i = 0; i < vectors.length; i++){
        var sphere = new THREE.Mesh(geometry,material);
        sphere.position.copy(vectors[i]);
        sphere.position.multiplyScalar(100);
        sphere.position.add(new THREE.Vector3(0,0,-15000));
        scenecontainer.add(sphere);
        spheresss.push(sphere);
    }
    center = new THREE.Mesh(geometry,material);
    center.position.add(new THREE.Vector3(0,0,-15000));
    scenecontainer.add(center);
}
var leveltwoload = function() {
    musicloop = loopsong("media/The Complex.ogg");
    musicloop.volume = 0.5;
    musicloop.pause();
client = new XMLHttpRequest();
client.open('GET', '/media/mapgen.txt');
client.onloadend = function() {
  levelparse(client.responseText);
  detectors();
}
client.send();
        setupBullets();
    setupRockets();
    setupColliders();
leveltwoscene = new THREE.Scene();
    leveltwocamera = new THREE.PerspectiveCamera(fov, screenWidth() / screenHeight(), 0.01, 100000);
    addcamera(leveltwocamera);
    skybox(leveltwoscene, 30);
    createShip();
    testmesh.material = new THREE.MeshStandardMaterial({
        color: 0xaaaaaa,
        shading: THREE.SmoothShading,
        metalness: 1,
        opacity:0.75,
        transparent:true
    })
    testmesh.add(new THREE.PointLight( 0xffff00, 1, 10000 ));
    var conegeometry = new THREE.ConeGeometry(0.005,0.02,3);
    conegeometry.rotateX(Math.PI/2);
    var conematerial = new THREE.MeshStandardMaterial({color: 0x00aaff});
    cone = new THREE.Mesh(conegeometry,conematerial);
    cone.position.z = -0.1;
    cone.position.y = -0.02;
    leveltwoscene.add(cone);
    conedummy = new THREE.Object3D();
    conedummy.position.z = -0.1;
    conedummy.position.y = -0.02;
    testmesh.add(conedummy);
            scenecontainer = new THREE.Object3D();
leveltwoscene.add(scenecontainer);
        createPauseMenu();
    currentpart = 0;
    if (pointerlocked == true) {
        leveltwounpause();
    } else {
        leveltwopause();
    }
    }
    var leveltwounload = function () {
        
    musicloop.pause();
    destroyShip();
    destroyPauseMenu();
}
var leveltwopause = function () {
    pause();
}
var leveltwounpause = function () {
    unpause();
}
