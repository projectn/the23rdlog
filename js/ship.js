function createShip(){
    test = new THREE.Geometry();
    v1 = new THREE.Vector3(-0.05, -0.05, -0.1);
    v2 = new THREE.Vector3(0.05, -0.05, -0.1);
    v3 = new THREE.Vector3(-0.1, -0.1, 0);
    v4 = new THREE.Vector3(0.1, -0.1, 0);
    v5 = new THREE.Vector3(-0.1, 0.1, 0);
    v6 = new THREE.Vector3(0.1, 0.1, 0);
    v7 = new THREE.Vector3(-0.045, -0.05, -0.1);
    v8 = new THREE.Vector3(0.045, -0.05, -0.1);
    v9 = new THREE.Vector3(-0.095, 0.1, 0);
    v10 = new THREE.Vector3(0.095, 0.1, 0);
    test.vertices.push(v1);
    test.vertices.push(v2);
    test.vertices.push(v3);
    test.vertices.push(v4);
    test.vertices.push(v5);
    test.vertices.push(v6);
    test.vertices.push(v7);
    test.vertices.push(v8);
    test.vertices.push(v9);
    test.vertices.push(v10);
    test.faces.push(new THREE.Face3(3, 1, 2));
    test.faces.push(new THREE.Face3(0, 2, 1));
    test.faces.push(new THREE.Face3(0, 6, 8));
    test.faces.push(new THREE.Face3(0, 8, 4));
    test.faces.push(new THREE.Face3(7, 1, 5));
    test.faces.push(new THREE.Face3(7, 5, 9));
    //test.faces.push(new THREE.Face3(0,4,2));
    //test.faces.push(new THREE.Face3(1,3,5));
    //test.faces.push(new THREE.Face3(6,7,9));
    //test.faces.push(new THREE.Face3(6,9,8));
    test.computeFaceNormals();
    engineloop = looptwo("media/engine.wav");
    //engineloop.playbackRate = 0.5;
    throttleoutlinegeom = new THREE.Geometry();
    throttleoutlinegeom.vertices.push(new THREE.Vector3(-0.04,0.05,-0.1));
    throttleoutlinegeom.vertices.push(new THREE.Vector3(-0.05,0.05,-0.1));
    throttleoutlinegeom.vertices.push(new THREE.Vector3(-0.05,0.05,-0.1));
    throttleoutlinegeom.vertices.push(new THREE.Vector3(-0.05,-0.05,-0.1));
    throttleoutlinegeom.vertices.push(new THREE.Vector3(-0.05,-0.05,-0.1));
    throttleoutlinegeom.vertices.push(new THREE.Vector3(-0.04,-0.05,-0.1));
    throttleoutlinegeom.vertices.push(new THREE.Vector3(-0.04,-0.05,-0.1));
    throttleoutlinegeom.vertices.push(new THREE.Vector3(-0.04,0.05,-0.1));
    throttleoutline = new THREE.LineSegments(throttleoutlinegeom,new THREE.LineBasicMaterial({color:0x29b6f6,transparent:true,opacity:0.5}));
    throttleoutline.position.x = -0.045;
    throttleoutline.position.z = -0.1;
    throttlemesh = new THREE.Mesh(new THREE.PlaneGeometry(0.005,0.05),new THREE.MeshBasicMaterial({color:0x29b6f6,transparent:true,opacity:0.5,side:THREE.DoubleSide}));
    throttlemesh.position.x = -0.045;
    throttlemesh.position.z = -0.1;
    
    mainoutlinegeom = new THREE.Geometry();
    mainoutlinegeom.vertices.push(new THREE.Vector3(-0.04,0.05,-0.1));
    mainoutlinegeom.vertices.push(new THREE.Vector3(-0.05,0.05,-0.1));
    mainoutlinegeom.vertices.push(new THREE.Vector3(-0.05,0.05,-0.1));
    mainoutlinegeom.vertices.push(new THREE.Vector3(-0.05,-0.05,-0.1));
    mainoutlinegeom.vertices.push(new THREE.Vector3(-0.05,-0.05,-0.1));
    mainoutlinegeom.vertices.push(new THREE.Vector3(-0.04,-0.05,-0.1));
    mainoutlinegeom.vertices.push(new THREE.Vector3(-0.04,-0.05,-0.1));
    mainoutlinegeom.vertices.push(new THREE.Vector3(-0.04,0.05,-0.1));
    mainoutline = new THREE.LineSegments(mainoutlinegeom,new THREE.LineBasicMaterial({color:0x00ff00,transparent:true,opacity:0.5}));
    mainoutline.position.x = -0.065;
    mainoutline.position.z = -0.1;
    mainmesh = new THREE.Mesh(new THREE.PlaneGeometry(0.005,0.05),new THREE.MeshBasicMaterial({color:0x00ff00,transparent:true,opacity:0.5,side:THREE.DoubleSide}));
    mainmesh.position.x = -0.055;
    mainmesh.position.z = -0.1;
    /*
    retrooutlinegeom = new THREE.Geometry();
    retrooutlinegeom.vertices.push(new THREE.Vector3(-0.04,0.05,-0.1));
    retrooutlinegeom.vertices.push(new THREE.Vector3(-0.05,0.05,-0.1));
    retrooutlinegeom.vertices.push(new THREE.Vector3(-0.05,0.05,-0.1));
    retrooutlinegeom.vertices.push(new THREE.Vector3(-0.05,-0.05,-0.1));
    retrooutlinegeom.vertices.push(new THREE.Vector3(-0.05,-0.05,-0.1));
    retrooutlinegeom.vertices.push(new THREE.Vector3(-0.04,-0.05,-0.1));
    retrooutlinegeom.vertices.push(new THREE.Vector3(-0.04,-0.05,-0.1));
    retrooutlinegeom.vertices.push(new THREE.Vector3(-0.04,0.05,-0.1));
    retrooutline = new THREE.LineSegments(retrooutlinegeom,new THREE.LineBasicMaterial({color:0x00ff00,transparent:true,opacity:0.5}));
    retrooutline.position.x = -0.085;
    retrooutline.position.z = -0.1;
    retromesh = new THREE.Mesh(new THREE.PlaneGeometry(0.005,0.05),new THREE.MeshBasicMaterial({color:0x00ff00,transparent:true,opacity:0.5,side:THREE.DoubleSide}));
    retromesh.position.x = -0.065;
    retromesh.position.z = -0.1;*/
    
    
    testmesh = new THREE.Mesh(test, new THREE.MeshStandardMaterial({
        color: 0xaaaaaa,
        shading: THREE.SmoothShading,
        metalness: 1
    }));
    testmesh.add(throttlemesh);
    testmesh.add(throttleoutline);
    testmesh.name = "singleplayer";
    testmesh.lastKnownPosition = function(){
        scenecontainer.position.add(velocity.divideScalar(10));
    }
    /*testmesh.add(retromesh);
    testmesh.add(retrooutline);
    */
    testmesh.add(mainmesh);
    testmesh.add(mainoutline);
    light = new THREE.PointLight(0xffffff, 1, 0.5);
    reticlemap = new THREE.ImageUtils.loadTexture("media/reticle.png");
    reticlemat = new THREE.SpriteMaterial({map:reticlemap,color:0x29b6f6});
    reticle = new THREE.Sprite(reticlemat);
    reticle.position.z = -10;

    targetmap = new THREE.ImageUtils.loadTexture("media/target.png");
    targetmat = new THREE.SpriteMaterial({map:targetmap,color:0x29b6f6});
    targetobj = new THREE.Sprite(targetmat);
    //targetobj.position.z = -12;
    //targetobj.visible = false;
    /*a = 0.2;
    b = 0.1;
    c = 0.25;
    reticlemat = new THREE.MeshBasicMaterial({color:0x29b6f6});
    reticlegeom = new THREE.Geometry();
    reticlegeom.vertices.push(new THREE.Vector3(-1,1,0));
    reticlegeom.vertices.push(new THREE.Vector3(-1,1-a,0));
    reticlegeom.vertices.push(new THREE.Vector3(-c,c,0));
    reticlegeom.vertices.push(new THREE.Vector3(-1+a,1,0));

    reticlegeom.vertices.push(new THREE.Vector3(1,1,0));
    reticlegeom.vertices.push(new THREE.Vector3(1,1-a,0));
    reticlegeom.vertices.push(new THREE.Vector3(c,c,0));
    reticlegeom.vertices.push(new THREE.Vector3(1-a,1,0));

    reticlegeom.vertices.push(new THREE.Vector3(1,-1,0));
    reticlegeom.vertices.push(new THREE.Vector3(1,-1+a,0));
    reticlegeom.vertices.push(new THREE.Vector3(c,-c,0));
    reticlegeom.vertices.push(new THREE.Vector3(1-a,-1,0));

    reticlegeom.vertices.push(new THREE.Vector3(-1,-1,0));
    reticlegeom.vertices.push(new THREE.Vector3(-1,-1+a,0));
    reticlegeom.vertices.push(new THREE.Vector3(-c,-c,0));
    reticlegeom.vertices.push(new THREE.Vector3(-1+a,-1,0));

    reticlegeom.vertices.push(new THREE.Vector3(0,b,0));
    reticlegeom.vertices.push(new THREE.Vector3(-b,0,0));
    reticlegeom.vertices.push(new THREE.Vector3(0,-b,0));
    reticlegeom.vertices.push(new THREE.Vector3(b,0,0));

    reticlegeom.faces.push(new THREE.Face3(0,1,2));
    reticlegeom.faces.push(new THREE.Face3(0,2,3));

    reticlegeom.faces.push(new THREE.Face3(4,6,5));
    reticlegeom.faces.push(new THREE.Face3(4,7,6));

    reticlegeom.faces.push(new THREE.Face3(8,9,10));
    reticlegeom.faces.push(new THREE.Face3(8,10,11));

    reticlegeom.faces.push(new THREE.Face3(12,14,13));
    reticlegeom.faces.push(new THREE.Face3(12,15,14));

    reticlegeom.faces.push(new THREE.Face3(16,17,18));
    reticlegeom.faces.push(new THREE.Face3(16,18,19));
    reticle = new THREE.Mesh(reticlegeom,reticlemat);
    reticle.scale.x = 0.05;
    reticle.scale.y = 0.05;
    reticle.position.z = -1;*/
    accelmap = new THREE.ImageUtils.loadTexture("media/accel.png");
    accelmat = new THREE.SpriteMaterial({map:accelmap,color:0xffffff,transparent:true});
    //accelmat = new THREE.MeshBasicMaterial({color:0x29b6f6,transparent:true});
    //accelgeom = new THREE.RingGeometry(0.2,0.35,30);
    //accelcircle = new THREE.Mesh(accelgeom,accelmat);
    //accelcircle.scale.x = 0.05;
    //accelcircle.scale.y = 0.05;
    //accelcircle.position.z = -1;
    //accel = new THREE.Object3D();
    //accel.add(accelcircle);
    accel = new THREE.Sprite(accelmat);
    accel.position.z = -11;
    testmesh.add(reticle);
    testmesh.add(accel);
    testmesh.add(light);
    //testmesh.add(targetobj);
    testmesh.add(window[levelname+"camera"]);
    testmesh.hull = 500;
    testmesh.factor = 100;
    testmesh.owner = "singleplayer";
    testmesh.hit = function(){
        mainmesh.scale.y = this.hull/500;
    }
    testmesh.destroy = function(){
        switchLevel("fail");
    }
    addCollider(testmesh);
    window[levelname+"scene"].add(testmesh);
    velocity = new THREE.Vector3(0,0,0);
    throttle = 0;
    throttleUp = 0;
    throttleDown = 0;
    retroThruster = 0;
    mainThruster = 0;
    leftThruster = 0;
    rightThruster = 0;
    topThruster = 0;
    bottomThruster = 0;
    retroThrusterOn = -1;
    mainThrusterOn = -1;
    leftThrusterOn = -1;
    rightThrusterOn = -1;
    topThrusterOn = -1;
    bottomThrusterOn = -1;
    targetmaterial = new THREE.MeshBasicMaterial({color:0xffffff,wireframe:true});
    counter = 5;
    rollLeft = 0;
    rollRight = 0;
    target = 0;
    testX = 0;
    testY = 0;
    testZ = 0;
    targeted = 0;
    movementX = 0;
    movementY = 0;
    shooting = false;
    document.addEventListener('mousemove', mouseMove, false);
    document.addEventListener('mousedown', mouseDown, false);
    document.addEventListener('mouseup', mouseUp, false);
    document.addEventListener('keydown',keyDown,false);
    document.addEventListener('keyup',keyUp,false);
}
function resetControls(){
    movementX = 0;
    movementY=0;
    accel.position.x = 0;
    accel.position.y = 0;
}
var mouseMove = function () {
    if (pointerlocked == false) return;
    movementX += event.movementX/sensitivity;
    movementY += event.movementY/sensitivity;
    if(movementX > 500){
        movementX = 500;
    }
    if(movementX < -500){
        movementX = -500;
    }
    if(movementY > 500){
        movementY = 500;
    }
    if(movementY < -500){
        movementY = -500;
    }
    accel.position.y = movementY/-500*11;
    accel.position.x = -movementX/-500*11;
    
}
var mouseDown = function(event){
    if(event.button == 0){
        if(pointerlocked == false) return;
        shooting = true;
        counter = 5;
    }
    /*if(event.button == 2){
    shootRocket(testmesh,velocity,"singleplayer",targeted);
}*/
    //testbullet.visible = true;
    /*var testbullet = new THREE.Line(bullet, new THREE.LineBasicMaterial({color:0xff00ff,linewidth:1}));
    //var testbullet = new THREE.Mesh(new THREE.SphereGeometry(1,32,32),new THREE.MeshNormalMaterial());
    testbullet.position.copy(testmesh.localToWorld(new THREE.Vector3(-5,0,0)));
    testbullet.time = 0;
    testbullet.velocity = testmesh.localToWorld(new THREE.Vector3(0,0,-5000));
    testbullet.position.sub(scenecontainer.position);
    testbullet.rotation.copy(testmesh.rotation);
    bullets.push(testbullet);
    scenecontainer.add(testbullet);
    var testbullet2 = new THREE.Line(bullet, new THREE.LineBasicMaterial({color:0xff00ff,linewidth:1}));
    //var testbullet = new THREE.Mesh(new THREE.SphereGeometry(1,32,32),new THREE.MeshNormalMaterial());
    testbullet2.position.copy(testmesh.localToWorld(new THREE.Vector3(5,0,0)));
    testbullet2.time = 0;
    testbullet2.velocity = testmesh.localToWorld(new THREE.Vector3(0,0,-5000));
    testbullet2.position.sub(scenecontainer.position);
    testbullet2.rotation.copy(testmesh.rotation);
    bullets.push(testbullet2);
    scenecontainer.add(testbullet2);*/
}
var mouseUp = function(event){
    if(event.button == 0){
    shooting = false;
}

}
var keyDown = function(event){
    if (event.altKey){
        return;
    }
    switch(event.keyCode){
        case 87: throttleUp = 1; break;
        case 83: throttleDown = 1; break;
        case 65: rollLeft = 1; break;
        case 68: rollRight = 1; break;
        case 84: target = 1; break;
    }
}
var keyUp = function(event){
    if (event.altKey){
        return;
    }
    switch(event.keyCode){
        case 87: throttleUp = 0; break;
        case 83: throttleDown = 0; break;
        case 65: rollLeft = 0; break;
        case 68: rollRight = 0; break;
        //Todo: Strafing
    }
}
function destroyShip(){
    engineloop.pause();
    document.removeEventListener('mousemove', mouseMove, false);
    document.removeEventListener('mousedown', mouseDown, false);
    document.removeEventListener('mousemove', mouseMove, false);
    document.removeEventListener('keydown', keyDown, false);
    document.removeEventListener('keyup', keyUp, false);
}
function updateControls(delta){
    if(target == 1){
        colliders.sort(function(a,b){
            apos = b.getWorldPosition();
            bpos = a.getWorldPosition();
            return Math.sqrt(bpos.x*bpos.x+bpos.y*bpos.y+bpos.z*bpos.z) - Math.sqrt(apos.x*apos.x+apos.y*apos.y+apos.z*apos.z);
        });
        for(var i = 0; i < colliders.length; i++){
            colliders[i].target.visible = false;
        }
        for(var i = 0; i < colliders.length; i++){
            window[levelname+"camera"].updateMatrix(); 
            window[levelname+"camera"].updateMatrixWorld(); 

            var frustum = new THREE.Frustum();

            frustum.setFromMatrix( new THREE.Matrix4().multiply( window[levelname+"camera"].projectionMatrix, window[levelname+"camera"].matrixWorldInverse ) );

            if(frustum.containsPoint( colliders[i].getWorldPosition() )){
                targeted = colliders[i];
                colliders[i].target.visible = true;
                target = 0;
                break;
            }
        }
        target = 0;
    }
    var here = false;
    for(var i = 0; i < colliders.length; i++){
        if(colliders[i] == targeted){
            here = true;
        }
    }
    if(here == false){
        targeted = 0;
    }
    if(shooting == true){
        counter++;
        if(counter > 5){
            counter = 0;

            shoot(testmesh,velocity,"singleplayer",2,600);
            playsong("media/blaster.wav");
            //testbullet1.material.opacity = Math.random()/2;
            //testbullet2.material.opacity = Math.random()/2;
        }
        
        }
    throttle += 75*delta*(throttleUp-throttleDown);
        if(throttle < 0){
            throttle = 0;
        }
        if(throttle > 100){
            throttle = 100;
        }
        testX += movementX;
        testY += movementY;
        window[levelname+"camera"].rotateX(movementY * -0.0003*delta);
        window[levelname+"camera"].rotateY(movementX * -0.0003*delta);
     testZ += (rollRight-rollLeft)* delta;
        window[levelname+"camera"].rotateZ((rollRight-rollLeft)*-0.3*delta);
}
function updateShip(delta){
    if (pointerlocked == true) {
        if(Math.sqrt(accel.position.x*accel.position.x + accel.position.y*accel.position.y) < 1){
        accel.material.opacity = 0.75*(Math.sqrt(accel.position.x*accel.position.x + accel.position.y*accel.position.y)/1)+0.25;
    }
    else{
        accel.material.opacity = 1;
    }
        throttlemesh.scale.y = throttle/100;
        /*if(retroThruster > 2){
            retromesh.scale.y = retroThruster/100;
        }
        else{
            retromesh.scale.y = 0;
        }
        if(mainThruster > 2){
            mainmesh.scale.y = mainThruster/100;
        }
        else{
            mainmesh.scale.y = 0;
        }*/
        
        
        //speed = 100*Math.sqrt(velocity.x*velocity.x + velocity.y*velocity.y + velocity.z*velocity.z);
        /*if(throttle>speed){
            decreaseBackThrusters = 1;
            increaseBackThrusters = 0;
            increaseFrontThrusters = 1;
            decreaseFrontThrusters = 0;
        }
        if(throttle<speed){
            decreaseFrontThrusters = 1;
            increaseFrontThrusters = 0;
            increaseBackThrusters = 1;
            decreaseBackThrusters = 0;
        }
        if(throttle==speed){
            decreaseFrontThrusters = 1;
            increaseFrontThrusters = 0;
            decreaseBackThrusters = 1;
            increaseBackThrusters = 0;
        }
        backThrusters += 5*delta*(increaseBackThrusters - decreaseBackThrusters);
        frontThrusters += 5*delta*(increaseFrontThrusters - decreaseFrontThrusters);
        if(backThrusters < 0){
            backThrusters = 0;
        }
        if(backThrusters > 100){
            backThrusters = 1000;
        }
        if(frontThrusters < 0){
            frontThrusters = 0;
        }
        if(frontThrusters > 100){
            frontThrusters = 1000;
        }*/
       
        window[levelname+"camera"].rotation.x /= 39*delta;
        window[levelname+"camera"].rotation.y /= 39*delta;
        window[levelname+"camera"].rotation.z /= 39*delta;
        testX /= 48*delta;
        testY /= 48*delta;
        testZ /= 39*delta;
        testmesh.rotateX(testY * -0.003*delta);
        testmesh.rotateY(testX * -0.003*delta);
        testmesh.rotateZ(testZ*-6*delta);
        /*var direction = new THREE.Vector3();
        testmesh.getWorldDirection(direction);
        direction.multiplyScalar(delta*(Math.abs(frontThrusters-backThrusters)-backThrusters)/100);
        velocity.add(direction);
        velocity.divideScalar(1+(Math.abs(frontThrusters-backThrusters)/100)*delta);
        testmesh.position.addScaledVector(velocity,500*delta);*/
        var target = new THREE.Vector3(0,0,-throttle);
        var actual = new THREE.Vector3();
        //console.log(velocity.z);
        actual.copy(velocity);
        //console.log(actual.z);
        testmesh.worldToLocal(actual);
        //console.log(actual.z);
        scenecontainer.position.sub(velocity.divideScalar(10));
        ////console.log(actual.x);
        var targetX = target.x - actual.x;
        var targetY = target.y - actual.y;
        var targetZ = target.z - actual.z;
        //console.log(actual.z);
        actual.x+=(leftThruster*delta*0.25)-(rightThruster*delta*0.25);
        ////console.log(actual.x);
        actual.y+=(bottomThruster*delta*0.25)-(topThruster*delta*0.25);
        actual.z+=(retroThruster*delta*0.5)-(mainThruster*delta*0.5);
        //console.log(actual.z);
        retroThruster += retroThrusterOn*20*delta;
        mainThruster += mainThrusterOn*20*delta;
        leftThruster += leftThrusterOn*1000*delta;
        rightThruster += rightThrusterOn*1000*delta;
        topThruster += topThrusterOn*1000*delta;
        bottomThruster += bottomThrusterOn*1000*delta;
        if(retroThruster < 0){
            retroThruster = 0;
        }
        if(retroThruster > 100){
            retroThruster = 100;
        }
        if(mainThruster < 0){
            mainThruster = 0;
        }
        if(mainThruster > 100){
            mainThruster = 100;
        }
        if(leftThruster < 0){
            leftThruster = 0;
        }
        if(leftThruster > 100){
            leftThruster = 100;
        }
        if(rightThruster < 0){
            rightThruster = 0;
        }
        if(rightThruster > 100){
            rightThruster = 100;
        }
        if(topThruster < 0){
            topThruster = 0;
        }
        if(topThruster > 100){
            topThruster = 100;
        }
        if(bottomThruster < 0){
            bottomThruster = 0;
        }
        if(bottomThruster > 100){
            bottomThruster = 100;
        }
        if(actual.x == 0){
            ////console.log("we're good!");
        }
        else{
            if(Math.abs(actual.x) < delta*0.25*(100/3)){
                ////console.log("within 1/30 of target");
                rightThrusterOn = -1;
                leftThrusterOn = -1;
                if(actual.x > 0 && rightThruster == 0){
                    rightThruster = actual.x/(0.25*delta);
                }
                else if(leftThruster == 0){
                    leftThruster = -actual.x/(0.25*delta);
                }
            }
            else{
                //if "areas" of both, powered off, is within 1/30, then turn both OFF.
                if(Math.abs(rightarea()+leftarea()+actual.x) < delta*0.25*(100/3)){
                    leftThrusterOn = -1;
                    rightThrusterOn = -1;
                }
                else{
                    direction = Math.abs(actual.x)/actual.x;
                    if(direction == 1){
                        if(rightonemore()+leftarea()+actual.x < 0){
                            leftThrusterOn = -1;
                            rightThrusterOn = -1;
                        }
                        else{
                            leftThrusterOn = -1;
                            rightThrusterOn = 1;
                        }
                    }
                    if(direction == -1){
                        if(rightarea()+leftonemore()+actual.x > 0){
                            leftThrusterOn = -1;
                            rightThrusterOn = -1;
                        }
                        else{
                            leftThrusterOn = 1;
                            rightThrusterOn = -1;
                        }
                    }
                }
            }
        }



        //Top and Bottom
        if(actual.y == 0){
            ////console.log("we're good!");
        }
        else{
            if(Math.abs(actual.y) < delta*0.25*(100/3)){
                ////console.log("within 1/30 of target");
                topThrusterOn = -1;
                bottomThrusterOn = -1;
                if(actual.y > 0 && topThruster == 0){
                    topThruster = actual.y/(0.25*delta);
                }
                else if(bottomThruster == 0){
                    bottomThruster = -actual.y/(0.25*delta);
                }
            }
            else{
                //if "areas" of both, powered off, is within 1/30, then turn both OFF.
                if(Math.abs(bottomarea()+toparea()+actual.y) < delta*0.25*(100/3)){
                    bottomThrusterOn = -1;
                    topThrusterOn = -1;
                }
                else{
                    direction = Math.abs(actual.y)/actual.y;
                    if(direction == 1){
                        if(toponemore()+bottomarea()+actual.y < 0){
                            bottomThrusterOn = -1;
                            topThrusterOn = -1;
                        }
                        else{
                            bottomThrusterOn = -1;
                            topThrusterOn = 1;
                        }
                    }
                    if(direction == -1){
                        if(toparea()+bottomonemore()+actual.y > 0){
                            bottomThrusterOn = -1;
                            topThrusterOn = -1;
                        }
                        else{
                            bottomThrusterOn = 1;
                            topThrusterOn = -1;
                        }
                    }
                }
            }
        }

        //console.log(actual.z);
        //Main and Retro
        if(actual.z == target.z){
            //console.log("we're good!");
        }
        else{
            if(Math.abs(actual.z-target.z) < delta*0.5*(2/3)){
                //console.log("within 1/30 of target");
                mainThrusterOn = -1;
                retroThrusterOn = -1;
                if(-targetZ > 0 && mainThruster == 0){
                    mainThruster = -targetZ/(0.5*delta);
                }
                else if(retroThruster == 0){
                    retroThruster = targetZ/(0.5*delta);
                }
            }
            else{
                //if "areas" of both, powered off, is within 1/30, then turn both OFF.
                if(Math.abs(mainarea()+retroarea()+actual.z-target.z) < delta*0.5*(2/3)){
                    retroThrusterOn = -1;
                    mainThrusterOn = -1;
                }
                else{
                    direction = Math.abs(actual.z-target.z)/(actual.z-target.z);
                    if(direction == 1){
                        if(mainonemore()+retroarea()+actual.z < target.z){
                            //console.log("whaaaa");
                            retroThrusterOn = -1;
                            mainThrusterOn = -1;
                        }
                        else{
                            retroThrusterOn = -1;
                            mainThrusterOn = 1;
                        }
                    }
                    if(direction == -1){
                        if(mainarea()+retroonemore()+actual.z > target.z){
                            //console.log("whaaaa");
                            retroThrusterOn = -1;
                            mainThrusterOn = -1;
                        }
                        else{
                            retroThrusterOn = 1;
                            mainThrusterOn = -1;
                        }
                    }
                }
            }
        }
        //console.log(actual.z);
        ////console.log(actual.x);
        testmesh.localToWorld(actual);
        //console.log(actual.z);
        ////console.log(actual.x);
        velocity.copy(actual);
        //console.log(velocity.z);
        ////console.log(velocity.x);
        ////console.log("Z:"+actual.z);
        /*if(0.5*(bottomThruster*(bottomThruster/1000))*0.25>=targetY){
            bottomThrusterOn = -1;
        }
//        else if(targetY > 0.3){
//            bottomThrusterOn = 1;
//        }
        else{
            bottomThrusterOn = 1;
        }
        if(0.5*(topThruster*(topThruster/1000))*0.25>= -targetY){
            topThrusterOn = -1;
        }
//        else if(-targetY > 0.3){
//            topThrusterOn = 1;
//        }
        else{
            topThrusterOn = 1;
        }
         if(0.5*(rightThruster*(rightThruster/1000))*0.25>=-targetX){
            rightThrusterOn = -1;
        }
//        else if(-targetX > 0.3){
//            rightThrusterOn = 1;
//        }
        else{
            rightThrusterOn = 1;
        }
         if(0.5*(leftThruster*(leftThruster/1000))*0.25>=targetX){
            leftThrusterOn = -1;
        }
//        else if(targetX > 0.3){
//            leftThrusterOn = 1;
//        }
        else{
            leftThrusterOn = 1;
        }
         if(0.5*(retroThruster*(retroThruster/20))*0.5>=targetZ){
            retroThrusterOn = -1;
        }
//        else if(targetZ > 0.02){
//            retroThrusterOn = 1;
//        }
        else{
            retroThrusterOn = 1;
        }
         if(0.5*(mainThruster*(mainThruster/20))*0.5>=-targetZ){
            mainThrusterOn = -1;
        }
        
//        else if(-targetZ > 0.02){
//            mainThrusterOn = 1;
//        }
        else{
            mainThrusterOn = 1;
        }
        retroThruster += retroThrusterOn*20*delta;
        mainThruster += mainThrusterOn*20*delta;
        leftThruster += leftThrusterOn*1000*delta;
        rightThruster += rightThrusterOn*1000*delta;
        topThruster += topThrusterOn*1000*delta;
        bottomThruster += bottomThrusterOn*1000*delta;
        actual.x+=(leftThruster*delta*0.25)-(rightThruster*delta*0.25);
        actual.y+=(bottomThruster*delta*0.25)-(topThruster*delta*0.25);
        actual.z+=(retroThruster*delta*0.5)-(mainThruster*delta*0.5);
        //console.log(Math.abs(actual.x));
        testmesh.localToWorld(actual);
        velocity.copy(actual);
        testmesh.worldToLocal(actual);
        if(Math.abs(actual.x - target.x)<=0.1){
            ////console.log("hiiii");
            actual.x = target.x;
        }
        if(Math.abs(actual.y - target.y)<=0.1){
            ////console.log("hiiii");
            actual.y = target.y;
        }
        if(Math.abs(actual.z - target.z)<=0.02){
            ////console.log("hiiiiE");
            actual.z = target.z;
        }
        testmesh.localToWorld(actual);
        if(retroThruster < 0){
            retroThruster = 0;
        }
        if(retroThruster > 100){
            retroThruster = 100;
        }
        if(mainThruster < 0){
            mainThruster = 0;
        }
        if(mainThruster > 100){
            mainThruster = 100;
        }
        if(leftThruster < 0){
            leftThruster = 0;
        }
        if(leftThruster > 100){
            leftThruster = 100;
        }
        if(rightThruster < 0){
            rightThruster = 0;
        }
        if(rightThruster > 100){
            rightThruster = 100;
        }
        if(topThruster < 0){
            topThruster = 0;
        }
        if(topThruster > 100){
            topThruster = 100;
        }
        if(bottomThruster < 0){
            bottomThruster = 0;
        }
        if(bottomThruster > 100){
            bottomThruster = 100;
        }
        //console.log(topThruster+":"+mainThruster+":")
        */

        //scenecontainer.position.sub(actual.divideScalar(10));
        //testmesh.translateZ(-forward*delta);
    }
}
function leftarea(){
    var area = 0;
    var n = leftThruster/(delta*1000);
    var sum = (n*(n+1))/2;
    area = sum *delta*1000 * delta*0.25;
    /*for(var x = leftThruster; x > 0; x -= delta*1000){
        area += x*delta*0.25;
    }*/
    return area;
}
function leftonemore(){
    var area = leftarea();
    area += leftThruster*delta*0.25;
    var neww = leftThruster + delta*1000;
    if(neww > 100){
        neww = 100;
    }
    area += neww*delta*0.25;
    return area;
}
function rightarea(){
    var area = 0;
    var n = rightThruster/(delta*1000);
    var sum = (n*(n+1))/2;
    area = -sum *delta*1000 * delta*0.25;
    return area;
}
function rightonemore(){
    var area = rightarea();
    area -= rightThruster*delta*0.25;
    var neww = rightThruster + delta*1000;
    if(neww > 100){
        neww = 100;
    }
    area -= neww*delta*0.25;
    return area;
}
function bottomarea(){
    var area = 0;
    var n = bottomThruster/(delta*1000);
    var sum = (n*(n+1))/2;
    area = sum *delta*1000 * delta*0.25;
    return area;
}
function bottomonemore(){
    var area = bottomarea();
    area += bottomThruster*delta*0.25;
    var neww = bottomThruster + delta*1000;
    if(neww > 100){
        neww = 100;
    }
    area += neww*delta*0.25;
    return area;
}
function toparea(){
    var area = 0;
    var n = topThruster/(delta*1000);
    var sum = (n*(n+1))/2;
    area = -sum *delta*1000 * delta*0.25;
    return area;
}
function toponemore(){
    var area = toparea();
    area -= topThruster*delta*0.25;
    var neww = topThruster + delta*1000;
    if(neww > 100){
        neww = 100;
    }
    area -= neww*delta*0.25;
    return area;
}
function retroarea(){
    var area = 0;
    var n = retroThruster/(delta*20);
    var sum = (n*(n+1))/2;
    area = sum *delta*20 * delta*0.5;
    return area;
}
function retroonemore(){
    var area = retroarea();
    area += retroThruster*delta*0.5;
    var neww = retroThruster + delta*20;
    if(neww > 100){
        neww = 100;
    }
    area += neww*delta*0.5;
    return area;
}
function mainarea(){
    var area = 0;
    var n = mainThruster/(delta*20);
    var sum = (n*(n+1))/2;
    area = -sum *delta*20 * delta*0.5;
    return area;
}
function mainonemore(){
    var area = mainarea();
    area -= mainThruster*delta*0.5;
    var neww = mainThruster + delta*20;
    if(neww > 100){
        neww = 100;
    }
    area -= neww*delta*0.5;
    return area;
}