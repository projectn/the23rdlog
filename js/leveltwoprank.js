var levelone = function (delta) {
    if (scenecontainer.position.x > limit) {
        scenecontainer.position.x = limit;
    } else if (scenecontainer.position.x < (limit * -1)) {
        scenecontainer.position.x = (limit * -1);
    }
    if (scenecontainer.position.y > limit) {
        scenecontainer.position.y = limit;
    } else if (scenecontainer.position.y < (limit * -1)) {
        scenecontainer.position.y = (limit * -1);
    }
    if (scenecontainer.position.z > limit) {
        scenecontainer.position.z = limit;
    } else if (scenecontainer.position.z < (limit * -1)) {
        scenecontainer.position.z = (limit * -1);
    }
    if (sound) {
        musicloop.volume = 1;
    } else {
        musicloop.volume = 0;
    }
    if (pointerlocked == true) {
        
        if(currentpart == 0){
            vox = playsongcallback("media/introlevel1.ogg",function(){currentpart = 2;resetControls();});
            musicloop.play();
            currentpart = 1;
        }
        if(currentpart < 2){
             renderer.render(starscene,starcamera);
             starrotator.rotateX(-0.01 * delta);
             //starrotator.rotation.y -= 0.05 * delta;
        }
        if(currentpart == 2){
            throttle = 20;
            vox = playsongcallback("media/levelone_1.ogg",function(){currentpart = 4;resetControls();});
            currentpart = 3;
        }
        if(currentpart == 3 || currentpart == 7 || currentpart == 11){
            resetControls();
        }
        if(currentpart == 4){
            if(movementX != 0){
                currentpart = 5;
                setTimeout(function(){currentpart = 6;},5000);
            }
        }
        if(currentpart == 6){
            vox = playsongcallback("media/levelone_2.ogg",function(){currentpart = 8;resetControls();});
            currentpart = 7;
        }
        if(currentpart == 8){
            if(rollLeft-rollRight != 0){
                currentpart = 9;
                setTimeout(function(){currentpart = 10;},5000);
            }
        }
        if(currentpart == 10){
            vox = playsongcallback("media/levelone_3.ogg",function(){currentpart = 12;resetControls();});
            spawnRings();
            currentpart = 11;
        }
        if(currentpart == 11){
            throttle -= 5;
            if(throttle < 0){
                throttle = 0;
            }
        }
        if(currentpart == 12){
            ring1.material.color = new THREE.Color(0xffff00);
            if(ring1.getWorldPosition().length() < 10){
                ring1.material.color = new THREE.Color(0x00ff00);
                currentpart = 13;
            }
        }
        if(currentpart == 13){
            ring2.material.color = new THREE.Color(0xffff00);
            if(ring2.getWorldPosition().length() < 10){
                ring2.material.color = new THREE.Color(0x00ff00);
                currentpart = 14;
            }
        }
        if(currentpart == 14){
            ring3.material.color = new THREE.Color(0xffff00);
            if(ring3.getWorldPosition().length() < 10){
                ring3.material.color = new THREE.Color(0x00ff00);
                currentpart = 15;
            }
        }
        if(currentpart == 15){
            ring4.material.color = new THREE.Color(0xffff00);
            if(ring4.getWorldPosition().length() < 10){
                ring4.material.color = new THREE.Color(0x00ff00);
                currentpart = 16;
            }
        }
        if(currentpart == 16){
            ring5.material.color = new THREE.Color(0xffff00);
            if(ring5.getWorldPosition().length() < 10){
                ring5.material.color = new THREE.Color(0x00ff00);
                currentpart = 17;
            }
        }
        if(currentpart == 17){
            ring6.material.color = new THREE.Color(0xffff00);
            if(ring6.getWorldPosition().length() < 10){
                ring6.material.color = new THREE.Color(0x00ff00);
                currentpart = 18;
            }
        }
        if(currentpart == 18){
            ring7.material.color = new THREE.Color(0xffff00);
            if(ring7.getWorldPosition().length() < 10){
                ring7.material.color = new THREE.Color(0x00ff00);
                currentpart = 19;
            }
        }
        if(currentpart == 19){
            ring8.material.color = new THREE.Color(0xffff00);
            if(ring8.getWorldPosition().length() < 10){
                ring8.material.color = new THREE.Color(0x00ff00);
                currentpart = 20;
            }
        }
        if(currentpart == 20){
            ring9.material.color = new THREE.Color(0xffff00);
            if(ring9.getWorldPosition().length() < 10){
                ring9.material.color = new THREE.Color(0x00ff00);
                currentpart = 21;
            }
        }
        if(currentpart == 21){
            ring10.material.color = new THREE.Color(0xffff00);
            if(ring10.getWorldPosition().length() < 10){
                ring10.material.color = new THREE.Color(0x00ff00);
                currentpart = 22;
            }
        }
        if(currentpart == 22){
            ring11.material.color = new THREE.Color(0xffff00);
            if(ring11.getWorldPosition().length() < 10){
                ring11.material.color = new THREE.Color(0x00ff00);
                currentpart = 23;
            }
        }
        if(currentpart == 23){
            ring12.material.color = new THREE.Color(0xffff00);
            if(ring12.getWorldPosition().length() < 10){
                ring12.material.color = new THREE.Color(0x00ff00);
                currentpart = 24;
            }
        }
        if(currentpart == 24){
            ring13.material.color = new THREE.Color(0xffff00);
            if(ring13.getWorldPosition().length() < 10){
                ring13.material.color = new THREE.Color(0x00ff00);
                currentpart = 25;
            }
        }
        if(currentpart == 25){
            ring14.material.color = new THREE.Color(0xffff00);
            if(ring14.getWorldPosition().length() < 10){
                ring14.material.color = new THREE.Color(0x00ff00);
                currentpart = 26;
            }
        }
        if(currentpart == 26){
            ring15.material.color = new THREE.Color(0xffff00);
            if(ring15.getWorldPosition().length() < 10){
                ring15.material.color = new THREE.Color(0x00ff00);
                currentpart = 27;
            }
        }
        if(currentpart == 27){
            ring16.material.color = new THREE.Color(0xffff00);
            if(ring16.getWorldPosition().length() < 10){
                ring16.material.color = new THREE.Color(0x00ff00);
                currentpart = 28;
            }
        }
        if(currentpart == 28){
            ring17.material.color = new THREE.Color(0xffff00);
            if(ring17.getWorldPosition().length() < 10){
                ring17.material.color = new THREE.Color(0x00ff00);
                currentpart = 29;
            }
        }
        if(currentpart == 29){
            ring18.material.color = new THREE.Color(0xffff00);
            if(ring18.getWorldPosition().length() < 10){
                ring18.material.color = new THREE.Color(0x00ff00);
                currentpart = 30;
            }
        }
        if(currentpart == 30){
            ring19.material.color = new THREE.Color(0xffff00);
            if(ring19.getWorldPosition().length() < 10){
                ring19.material.color = new THREE.Color(0x00ff00);
                currentpart = 31;
            }
        }
        if(currentpart == 31){
            ring20.material.color = new THREE.Color(0xffff00);
            if(ring20.getWorldPosition().length() < 10){
                ring20.material.color = new THREE.Color(0x00ff00);
                currentpart = 32;
            }
        }
        if(currentpart == 32){
            ring21.material.color = new THREE.Color(0xffff00);
            if(ring21.getWorldPosition().length() < 10){
                ring21.material.color = new THREE.Color(0x00ff00);
                currentpart = 33;
            }
        }
        if(currentpart == 33){
            ring22.material.color = new THREE.Color(0xffff00);
            if(ring22.getWorldPosition().length() < 10){
                ring22.material.color = new THREE.Color(0x00ff00);
                currentpart = 34;
            }
        }
        if(currentpart == 34){
            ring23.material.color = new THREE.Color(0xffff00);
            if(ring23.getWorldPosition().length() < 10){
                ring23.material.color = new THREE.Color(0x00ff00);
                currentpart = 35;
            }
        }
        if(currentpart == 35){
            ring24.material.color = new THREE.Color(0xffff00);
            if(ring24.getWorldPosition().length() < 10){
                ring24.material.color = new THREE.Color(0x00ff00);
                currentpart = 36;
            }
        }
        if(currentpart == 36){
            ring25.material.color = new THREE.Color(0xffff00);
            if(ring25.getWorldPosition().length() < 10){
                ring25.material.color = new THREE.Color(0x00ff00);
                currentpart = 37;
            }
        }
        if(currentpart == 37){
            ring26.material.color = new THREE.Color(0xffff00);
            if(ring26.getWorldPosition().length() < 10){
                ring26.material.color = new THREE.Color(0x00ff00);
                currentpart = 38;
            }
        }
        if(currentpart == 38){
            ring27.material.color = new THREE.Color(0xffff00);
            if(ring27.getWorldPosition().length() < 10){
                ring27.material.color = new THREE.Color(0x00ff00);
                currentpart = 39;
            }
        }
        if(currentpart == 39){
            ring28.material.color = new THREE.Color(0xffff00);
            if(ring28.getWorldPosition().length() < 10){
                ring28.material.color = new THREE.Color(0x00ff00);
                currentpart = 40;
            }
        }
        if(currentpart == 40){
            ring29.material.color = new THREE.Color(0xffff00);
            if(ring29.getWorldPosition().length() < 10){
                ring29.material.color = new THREE.Color(0x00ff00);
                currentpart = 41;
            }
        }
        if(currentpart == 41){
            ring30.material.color = new THREE.Color(0xffff00);
            if(ring30.getWorldPosition().length() < 10){
                ring30.material.color = new THREE.Color(0x00ff00);
                currentpart = 42;
            }
        }
        if(currentpart == 42){
            ring31.material.color = new THREE.Color(0xffff00);
            if(ring31.getWorldPosition().length() < 10){
                ring31.material.color = new THREE.Color(0x00ff00);
                currentpart = 43;
            }
        }
        if(currentpart == 43){
            ring32.material.color = new THREE.Color(0xffff00);
            if(ring32.getWorldPosition().length() < 10){
                ring32.material.color = new THREE.Color(0x00ff00);
                currentpart = 44;
            }
        }
        if(currentpart == 44){
            ring33.material.color = new THREE.Color(0xffff00);
            if(ring33.getWorldPosition().length() < 10){
                ring33.material.color = new THREE.Color(0x00ff00);
                currentpart = 45;
            }
        }
        if(currentpart == 45){
            ring34.material.color = new THREE.Color(0xffff00);
            if(ring34.getWorldPosition().length() < 10){
                ring34.material.color = new THREE.Color(0x00ff00);
                currentpart = 46;
            }
        }
        if(currentpart == 46){
            ring35.material.color = new THREE.Color(0xffff00);
            if(ring35.getWorldPosition().length() < 10){
                ring35.material.color = new THREE.Color(0x00ff00);
                currentpart = 47;
            }
        }
        if(currentpart == 47){
            ring36.material.color = new THREE.Color(0xffff00);
            if(ring36.getWorldPosition().length() < 10){
                ring36.material.color = new THREE.Color(0x00ff00);
                currentpart = 48;
            }
        }
        if(currentpart == 48){
            ring37.material.color = new THREE.Color(0xffff00);
            if(ring37.getWorldPosition().length() < 10){
                ring37.material.color = new THREE.Color(0x00ff00);
                currentpart = 49;
            }
        }
        if(currentpart == 49){
            ring38.material.color = new THREE.Color(0xffff00);
            if(ring38.getWorldPosition().length() < 10){
                ring38.material.color = new THREE.Color(0x00ff00);
                currentpart = 50;
            }
        }
        if(currentpart == 50){
            ring39.material.color = new THREE.Color(0xffff00);
            if(ring39.getWorldPosition().length() < 10){
                ring39.material.color = new THREE.Color(0x00ff00);
                currentpart = 51;
            }
        }
        if(currentpart == 51){
            ring40.material.color = new THREE.Color(0xffff00);
            if(ring40.getWorldPosition().length() < 10){
                ring40.material.color = new THREE.Color(0x00ff00);
                currentpart = 52;
            }
        }
        if(currentpart == 52){
            ring41.material.color = new THREE.Color(0xffff00);
            if(ring41.getWorldPosition().length() < 10){
                ring41.material.color = new THREE.Color(0x00ff00);
                currentpart = 53;
            }
        }
        if(currentpart == 53){
            ring42.material.color = new THREE.Color(0xffff00);
            if(ring42.getWorldPosition().length() < 10){
                ring42.material.color = new THREE.Color(0x00ff00);
                currentpart = 54;
            }
        }
        if(currentpart == 54){
            ring43.material.color = new THREE.Color(0xffff00);
            if(ring43.getWorldPosition().length() < 10){
                ring43.material.color = new THREE.Color(0x00ff00);
                currentpart = 55;
            }
        }
        if(currentpart == 55){
            ring44.material.color = new THREE.Color(0xffff00);
            if(ring44.getWorldPosition().length() < 10){
                ring44.material.color = new THREE.Color(0x00ff00);
                currentpart = 56;
            }
        }
        if(currentpart == 56){
            ring45.material.color = new THREE.Color(0xffff00);
            if(ring45.getWorldPosition().length() < 10){
                ring45.material.color = new THREE.Color(0x00ff00);
                currentpart = 57;
            }
        }
        if(currentpart == 57){
            ring46.material.color = new THREE.Color(0xffff00);
            if(ring46.getWorldPosition().length() < 10){
                ring46.material.color = new THREE.Color(0x00ff00);
                currentpart = 58;
            }
        }
        if(currentpart == 58){
            ring47.material.color = new THREE.Color(0xffff00);
            if(ring47.getWorldPosition().length() < 10){
                ring47.material.color = new THREE.Color(0x00ff00);
                currentpart = 59;
            }
        }
        if(currentpart == 59){
            ring48.material.color = new THREE.Color(0xffff00);
            if(ring48.getWorldPosition().length() < 10){
                ring48.material.color = new THREE.Color(0x00ff00);
                currentpart = 60;
            }
        }
        if(currentpart == 60){
            ring49.material.color = new THREE.Color(0xffff00);
            if(ring49.getWorldPosition().length() < 10){
                ring49.material.color = new THREE.Color(0x00ff00);
                currentpart = 61;
            }
        }
        if(currentpart == 61){
            vox = playsong("media/blasters.ogg");
            spawnTargets();
            currentpart = 62;
        }
        if(currentpart == 62){
            if(targetsdestroyed >= 5){
                currentpart = 63;
            }
        }
        if(currentpart == 63){
            vox = playsong("media/rockets.ogg");
            spawnTargets();
            currentpart = 64;
        }
        if(currentpart == 64){
            if(targetsdestroyed >= 5){
                currentpart = 65;
            }
        }
        if(currentpart == 65){
            vox = playsong("media/scrimage.ogg");
            spawnScrimage();
            currentpart = 66;
        }
        if(currentpart == 66){
            //Check for course completion here.
            if(false){
                currentpart = 67;
            }
        }
        if(currentpart == 67){
            //Get message here.
            vox = playsong("media/attacked.ogg");
            currentpart = 68;
        }
        if(currentpart == 68){
            //Attack happens here.
            //Check for end of attack.
            if(false){
                currentpart = 69;
            }
        }
        if(currentpart == 69){
            //Destruction scene here.
            //Check for end of scene.
            if(false){
                currentpart = 70;
            }
        }
        if(currentpart == 70){
            //Switch to next level here.
        }
        if(currentpart == 4 || currentpart == 8 || currentpart == 5 || currentpart == 9 || currentpart >= 12){
            updateControls(delta);
            
        }
        if(currentpart > 61){
            updateColliders();
            checkCollider(testmesh);
        }
        if(currentpart > 2){
            updateShip(delta);
        }
        if(currentpart > 2){
            renderer.render(levelonescene,levelonecamera);
            updateBullets(delta);
            updateRockets(delta);
        }
    }
}
function spawnScrimage(){
    enemiesdestroyed = 0;
    var scrimagebuilder = new THREE.Object3D();
    scrimagebuilder.position.sub(scenecontainer.position);
    scrimagebuilder.rotation.copy(testmesh.rotation);
    var cube = new THREE.BoxGeometry(100,100,100);
    var material = new THREE.MeshStandardMaterial({
        color: 0xaaaaaa,
        shading: THREE.SmoothShading,
        metalness: 1,
        emissive:0x444444
    });
    cube1 = new THREE.Mesh(cube,material.clone());
    scrimagebuilder.translateZ(-1000);
    cube1.position = new THREE.Vector3(-50,-50,-50);
    cube2 = new THREE.Object3D();
    cube2.add(cube1);
    cube2.position.copy(scrimagebuilder.position);
    cube2.rotation.copy(scrimagebuilder.rotation);
    cube2.factor = 86;
    scenecontainer.add(cube2);
    addCollider(cube2);
}
function spawnTargets(){
    targetsdestroyed = 0;
    var targetbuilder = new THREE.Object3D();
    targetbuilder.position.sub(scenecontainer.position);
    targetbuilder.rotation.copy(testmesh.rotation);
    targetbuilder.rotateX(-Math.PI/2);
    var cylinder = new THREE.CylinderGeometry(10,10,2,32);
    var material = new THREE.MeshLambertMaterial({color:0xffffff,emissive:0x00aaff,transparent:true,opacity:1});
    target1 = new THREE.Mesh(cylinder,material.clone());
    target2 = new THREE.Mesh(cylinder,material.clone());
    target3 = new THREE.Mesh(cylinder,material.clone());
    target4 = new THREE.Mesh(cylinder,material.clone());
    target5 = new THREE.Mesh(cylinder,material.clone());
    targetbuilder.translateY(1000);
    target1.position.copy(targetbuilder.position);
    target1.rotation.copy(targetbuilder.rotation);
    targetbuilder.translateX(300);
    target2.position.copy(targetbuilder.position);
    target2.rotation.copy(targetbuilder.rotation);
    targetbuilder.translateX(-600);
    target3.position.copy(targetbuilder.position);
    target3.rotation.copy(targetbuilder.rotation);
    targetbuilder.translateX(300);
    targetbuilder.translateZ(300);
    target4.position.copy(targetbuilder.position);
    target4.rotation.copy(targetbuilder.rotation);
    targetbuilder.translateZ(-600);
    target5.position.copy(targetbuilder.position);
    target5.rotation.copy(targetbuilder.rotation);
    scenecontainer.add(target1);
    scenecontainer.add(target2);
    scenecontainer.add(target3);
    scenecontainer.add(target4);
    scenecontainer.add(target5);
    target1.hull = 100;
    target1.factor = 10;
    target2.hull = 100;
    target2.factor = 10;
    target3.hull = 100;
    target3.factor = 10;
    target4.hull = 100;
    target4.factor = 10;
    target5.hull = 100;
    target5.factor = 10;
    target1.hit = function(){
        this.material.opacity = this.hull/100;
    }
    target2.hit = function(){
        this.material.opacity = this.hull/100;
    }
    target3.hit = function(){
        this.material.opacity = this.hull/100;
    }
    target4.hit = function(){
        this.material.opacity = this.hull/100;
    }
    target5.hit = function(){
        this.material.opacity = this.hull/100;
    }
    target1.destroy = function(){
        targetsdestroyed++;
    }
    target2.destroy = function(){
        targetsdestroyed++;
    }
    target3.destroy = function(){
        targetsdestroyed++;
    }
    target4.destroy = function(){
        targetsdestroyed++;
    }
    target5.destroy = function(){
        targetsdestroyed++;
    }
    addCollider(target1);
    addCollider(target2);
    addCollider(target3);
    addCollider(target4);
    addCollider(target5);
}
function spawnRings(){
    var ringbuilder = new THREE.Object3D();
    ringbuilder.position.sub(scenecontainer.position);
    ringbuilder.rotation.copy(testmesh.rotation);
    /*var torus1 = new THREE.TorusGeometry(10,3,10,10);
    var material1 = new THREE.MeshBasicMaterial({color:0x00aaff,wireframe:true});
    var torus2 = new THREE.TorusGeometry(10,3,10,10);
    var material2 = new THREE.MeshBasicMaterial({color:0x00aaff,wireframe:true});
    var torus3 = new THREE.TorusGeometry(10,3,10,10);
    var material3 = new THREE.MeshBasicMaterial({color:0x00aaff,wireframe:true});
    var torus4 = new THREE.TorusGeometry(10,3,10,10);
    var material4 = new THREE.MeshBasicMaterial({color:0x00aaff,wireframe:true});
    var torus5 = new THREE.TorusGeometry(10,3,10,10);
    var material5 = new THREE.MeshBasicMaterial({color:0x00aaff,wireframe:true});
    var torus6 = new THREE.TorusGeometry(10,3,10,10);
    var material6 = new THREE.MeshBasicMaterial({color:0x00aaff,wireframe:true});
    var torus7 = new THREE.TorusGeometry(10,3,10,10);
    var material7 = new THREE.MeshBasicMaterial({color:0x00aaff,wireframe:true});
    var torus8 = new THREE.TorusGeometry(10,3,10,10);
    var material8 = new THREE.MeshBasicMaterial({color:0x00aaff,wireframe:true});
    var torus9 = new THREE.TorusGeometry(10,3,10,10);
    var material9 = new THREE.MeshBasicMaterial({color:0x00aaff,wireframe:true});
    var torus0 = new THREE.TorusGeometry(10,3,10,10);
    var material0 = new THREE.MeshBasicMaterial({color:0x00aaff,wireframe:true});*/
    var torus = new THREE.TorusGeometry(10,3,10,10);
    var material = new THREE.MeshBasicMaterial({color:0x00aaff,wireframe:true,transparent:true,opacity:0.5});
    ring1 = new THREE.Mesh(torus,material.clone());
    ring2 = new THREE.Mesh(torus,material.clone());
    ring3 = new THREE.Mesh(torus,material.clone());
    ring4 = new THREE.Mesh(torus,material.clone());
    ring5 = new THREE.Mesh(torus,material.clone());
    ring6 = new THREE.Mesh(torus,material.clone());
    ring7 = new THREE.Mesh(torus,material.clone());
    ring8 = new THREE.Mesh(torus,material.clone());
    ring9 = new THREE.Mesh(torus,material.clone());
    ring10 = new THREE.Mesh(torus,material.clone());
    ring11 = new THREE.Mesh(torus,material.clone());
    ring12 = new THREE.Mesh(torus,material.clone());
    ring13 = new THREE.Mesh(torus,material.clone());
    ring14 = new THREE.Mesh(torus,material.clone());
    ring15 = new THREE.Mesh(torus,material.clone());
    ring16 = new THREE.Mesh(torus,material.clone());
    ring17 = new THREE.Mesh(torus,material.clone());
    ring18 = new THREE.Mesh(torus,material.clone());
    ring19 = new THREE.Mesh(torus,material.clone());
    ring20 = new THREE.Mesh(torus,material.clone());
    ring21 = new THREE.Mesh(torus,material.clone());
    ring22 = new THREE.Mesh(torus,material.clone());
    ring23 = new THREE.Mesh(torus,material.clone());
    ring24 = new THREE.Mesh(torus,material.clone());
    ring25 = new THREE.Mesh(torus,material.clone());
    ring26 = new THREE.Mesh(torus,material.clone());
    ring27 = new THREE.Mesh(torus,material.clone());
    ring28 = new THREE.Mesh(torus,material.clone());
    ring29 = new THREE.Mesh(torus,material.clone());
    ring30 = new THREE.Mesh(torus,material.clone());
    ring31 = new THREE.Mesh(torus,material.clone());
    ring32 = new THREE.Mesh(torus,material.clone());
    ring33 = new THREE.Mesh(torus,material.clone());
    ring34 = new THREE.Mesh(torus,material.clone());
    ring35 = new THREE.Mesh(torus,material.clone());
    ring36 = new THREE.Mesh(torus,material.clone());
    ring37 = new THREE.Mesh(torus,material.clone());
    ring38 = new THREE.Mesh(torus,material.clone());
    ring39 = new THREE.Mesh(torus,material.clone());
    ring40 = new THREE.Mesh(torus,material.clone());
    ring41 = new THREE.Mesh(torus,material.clone());
    ring42 = new THREE.Mesh(torus,material.clone());
    ring43 = new THREE.Mesh(torus,material.clone());
    ring44 = new THREE.Mesh(torus,material.clone());
    ring45 = new THREE.Mesh(torus,material.clone());
    ring46 = new THREE.Mesh(torus,material.clone());
    ring47 = new THREE.Mesh(torus,material.clone());
    ring48 = new THREE.Mesh(torus,material.clone());
    ring49 = new THREE.Mesh(torus,material.clone());
    ringbuilder.translateZ(-500);
    ring1.position.copy(ringbuilder.position);
    ring1.rotation.copy(ringbuilder.rotation);
    ringbuilder.rotateY(-Math.PI/12);
    ringbuilder.translateZ(-150);
    ring2.position.copy(ringbuilder.position);
    ring2.rotation.copy(ringbuilder.rotation);
    ringbuilder.rotateY(-Math.PI/12);
    ringbuilder.translateZ(-150);;
    ring3.position.copy(ringbuilder.position);
    ring3.rotation.copy(ringbuilder.rotation);
    ringbuilder.rotateY(-Math.PI/12);
    ringbuilder.translateZ(-150);
    ring4.position.copy(ringbuilder.position);
    ring4.rotation.copy(ringbuilder.rotation);
    ringbuilder.rotateY(-Math.PI/12);
    ringbuilder.translateZ(-150);
    ring5.position.copy(ringbuilder.position);
    ring5.rotation.copy(ringbuilder.rotation);
    ringbuilder.rotateY(-Math.PI/12);
    ringbuilder.translateZ(-150);
    ring6.position.copy(ringbuilder.position);
    ring6.rotation.copy(ringbuilder.rotation);
    ringbuilder.rotateY(-Math.PI/12);
    ringbuilder.translateZ(-150);
    ring7.position.copy(ringbuilder.position);
    ring7.rotation.copy(ringbuilder.rotation);
    ringbuilder.rotateY(Math.PI/12);
    ringbuilder.translateZ(-150);
    ring8.position.copy(ringbuilder.position);
    ring8.rotation.copy(ringbuilder.rotation);
    ringbuilder.rotateY(Math.PI/12);
    ringbuilder.translateZ(-150);
    ring9.position.copy(ringbuilder.position);
    ring9.rotation.copy(ringbuilder.rotation);
    ringbuilder.rotateY(Math.PI/12);
    ringbuilder.translateZ(-150);
    ring10.position.copy(ringbuilder.position);
    ring10.rotation.copy(ringbuilder.rotation);
    ringbuilder.rotateY(Math.PI/12);
    ringbuilder.translateZ(-150);
    ring11.position.copy(ringbuilder.position);
    ring11.rotation.copy(ringbuilder.rotation);
    ringbuilder.rotateY(Math.PI/12);
    ringbuilder.translateZ(-150);
    ring12.position.copy(ringbuilder.position);
    ring12.rotation.copy(ringbuilder.rotation);
    ringbuilder.rotateY(Math.PI/12);
    ringbuilder.translateZ(-150);
    ring13.position.copy(ringbuilder.position);
    ring13.rotation.copy(ringbuilder.rotation);
    ringbuilder.rotateY(Math.PI/12);
    ringbuilder.translateZ(-150);
    ring14.position.copy(ringbuilder.position);
    ring14.rotation.copy(ringbuilder.rotation);
    ringbuilder.rotateY(Math.PI/12);
    ringbuilder.translateZ(-150);
    ring15.position.copy(ringbuilder.position);
    ring15.rotation.copy(ringbuilder.rotation);
    ringbuilder.rotateY(Math.PI/12);
    ringbuilder.translateZ(-150);
    ring16.position.copy(ringbuilder.position);
    ring16.rotation.copy(ringbuilder.rotation);
    ringbuilder.rotateY(Math.PI/12);
    ringbuilder.translateZ(-150);
    ring17.position.copy(ringbuilder.position);
    ring17.rotation.copy(ringbuilder.rotation);
    ringbuilder.rotateY(Math.PI/12);
    ringbuilder.translateZ(-150);
    ring18.position.copy(ringbuilder.position);
    ring18.rotation.copy(ringbuilder.rotation);
    ringbuilder.rotateY(Math.PI/12);
    ringbuilder.translateZ(-150);
    ring19.position.copy(ringbuilder.position);
    ring19.rotation.copy(ringbuilder.rotation);
    ringbuilder.rotateY(-Math.PI/12);
    ringbuilder.translateZ(-150);
    ring20.position.copy(ringbuilder.position);
    ring20.rotation.copy(ringbuilder.rotation);
    ringbuilder.rotateY(-Math.PI/12);
    ringbuilder.translateZ(-150);
    ring21.position.copy(ringbuilder.position);
    ring21.rotation.copy(ringbuilder.rotation);
    ringbuilder.rotateY(-Math.PI/12);
    ringbuilder.translateZ(-150);
    ring22.position.copy(ringbuilder.position);
    ring22.rotation.copy(ringbuilder.rotation);
    ringbuilder.rotateY(-Math.PI/12);
    ringbuilder.translateZ(-150);
    ring23.position.copy(ringbuilder.position);
    ring23.rotation.copy(ringbuilder.rotation);
    ringbuilder.rotateY(-Math.PI/12);
    ringbuilder.translateZ(-150);
    ring24.position.copy(ringbuilder.position);
    ring24.rotation.copy(ringbuilder.rotation);
    ringbuilder.rotateY(-Math.PI/12);
    ringbuilder.translateZ(-150);
    ring25.position.copy(ringbuilder.position);
    ring25.rotation.copy(ringbuilder.rotation);
    ringbuilder.rotateX(-Math.PI/12);
    ringbuilder.translateZ(-150);
    ring26.position.copy(ringbuilder.position);
    ring26.rotation.copy(ringbuilder.rotation);
    ringbuilder.rotateX(-Math.PI/12);
    ringbuilder.translateZ(-150);;
    ring27.position.copy(ringbuilder.position);
    ring27.rotation.copy(ringbuilder.rotation);
    ringbuilder.rotateX(-Math.PI/12);
    ringbuilder.translateZ(-150);
    ring28.position.copy(ringbuilder.position);
    ring28.rotation.copy(ringbuilder.rotation);
    ringbuilder.rotateX(-Math.PI/12);
    ringbuilder.translateZ(-150);
    ring29.position.copy(ringbuilder.position);
    ring29.rotation.copy(ringbuilder.rotation);
    ringbuilder.rotateX(-Math.PI/12);
    ringbuilder.translateZ(-150);
    ring30.position.copy(ringbuilder.position);
    ring30.rotation.copy(ringbuilder.rotation);
    ringbuilder.rotateX(-Math.PI/12);
    ringbuilder.translateZ(-150);
    ring31.position.copy(ringbuilder.position);
    ring31.rotation.copy(ringbuilder.rotation);
    ringbuilder.rotateX(Math.PI/12);
    ringbuilder.translateZ(-150);
    ring32.position.copy(ringbuilder.position);
    ring32.rotation.copy(ringbuilder.rotation);
    ringbuilder.rotateX(Math.PI/12);
    ringbuilder.translateZ(-150);
    ring33.position.copy(ringbuilder.position);
    ring33.rotation.copy(ringbuilder.rotation);
    ringbuilder.rotateX(Math.PI/12);
    ringbuilder.translateZ(-150);
    ring34.position.copy(ringbuilder.position);
    ring34.rotation.copy(ringbuilder.rotation);
    ringbuilder.rotateX(Math.PI/12);
    ringbuilder.translateZ(-150);
    ring35.position.copy(ringbuilder.position);
    ring35.rotation.copy(ringbuilder.rotation);
    ringbuilder.rotateX(Math.PI/12);
    ringbuilder.translateZ(-150);
    ring36.position.copy(ringbuilder.position);
    ring36.rotation.copy(ringbuilder.rotation);
    ringbuilder.rotateX(Math.PI/12);
    ringbuilder.translateZ(-150);
    ring37.position.copy(ringbuilder.position);
    ring37.rotation.copy(ringbuilder.rotation);
    ringbuilder.rotateX(Math.PI/12);
    ringbuilder.translateZ(-150);
    ring38.position.copy(ringbuilder.position);
    ring38.rotation.copy(ringbuilder.rotation);
    ringbuilder.rotateX(Math.PI/12);
    ringbuilder.translateZ(-150);
    ring39.position.copy(ringbuilder.position);
    ring39.rotation.copy(ringbuilder.rotation);
    ringbuilder.rotateX(Math.PI/12);
    ringbuilder.translateZ(-150);
    ring40.position.copy(ringbuilder.position);
    ring40.rotation.copy(ringbuilder.rotation);
    ringbuilder.rotateX(Math.PI/12);
    ringbuilder.translateZ(-150);
    ring41.position.copy(ringbuilder.position);
    ring41.rotation.copy(ringbuilder.rotation);
    ringbuilder.rotateX(Math.PI/12);
    ringbuilder.translateZ(-150);
    ring42.position.copy(ringbuilder.position);
    ring42.rotation.copy(ringbuilder.rotation);
    ringbuilder.rotateX(Math.PI/12);
    ringbuilder.translateZ(-150);
    ring43.position.copy(ringbuilder.position);
    ring43.rotation.copy(ringbuilder.rotation);
    ringbuilder.rotateX(-Math.PI/12);
    ringbuilder.translateZ(-150);
    ring44.position.copy(ringbuilder.position);
    ring44.rotation.copy(ringbuilder.rotation);
    ringbuilder.rotateX(-Math.PI/12);
    ringbuilder.translateZ(-150);
    ring45.position.copy(ringbuilder.position);
    ring45.rotation.copy(ringbuilder.rotation);
    ringbuilder.rotateX(-Math.PI/12);
    ringbuilder.translateZ(-150);
    ring46.position.copy(ringbuilder.position);
    ring46.rotation.copy(ringbuilder.rotation);
    ringbuilder.rotateX(-Math.PI/12);
    ringbuilder.translateZ(-150);
    ring47.position.copy(ringbuilder.position);
    ring47.rotation.copy(ringbuilder.rotation);
    ringbuilder.rotateX(-Math.PI/12);
    ringbuilder.translateZ(-150);
    ring48.position.copy(ringbuilder.position);
    ring48.rotation.copy(ringbuilder.rotation);
    ringbuilder.rotateX(-Math.PI/12);
    ringbuilder.translateZ(-150);
    ring49.position.copy(ringbuilder.position);
    ring49.rotation.copy(ringbuilder.rotation);
    scenecontainer.add(ring1);
    scenecontainer.add(ring2);
    scenecontainer.add(ring3);
    scenecontainer.add(ring4);
    scenecontainer.add(ring5);
    scenecontainer.add(ring6);
    scenecontainer.add(ring7);
    scenecontainer.add(ring8);
    scenecontainer.add(ring9);
    scenecontainer.add(ring10);
    scenecontainer.add(ring11);
    scenecontainer.add(ring12);
    scenecontainer.add(ring13);
    scenecontainer.add(ring14);
    scenecontainer.add(ring15);
    scenecontainer.add(ring16);
    scenecontainer.add(ring17);
    scenecontainer.add(ring18);
    scenecontainer.add(ring19);
    scenecontainer.add(ring20);
    scenecontainer.add(ring21);
    scenecontainer.add(ring22);
    scenecontainer.add(ring23);
    scenecontainer.add(ring24);
    scenecontainer.add(ring25);
    scenecontainer.add(ring26);
    scenecontainer.add(ring27);
    scenecontainer.add(ring28);
    scenecontainer.add(ring29);
    scenecontainer.add(ring30);
    scenecontainer.add(ring31);
    scenecontainer.add(ring32);
    scenecontainer.add(ring33);
    scenecontainer.add(ring34);
    scenecontainer.add(ring35);
    scenecontainer.add(ring36);
    scenecontainer.add(ring37);
    scenecontainer.add(ring38);
    scenecontainer.add(ring39);
    scenecontainer.add(ring40);
    scenecontainer.add(ring41);
    scenecontainer.add(ring42);
    scenecontainer.add(ring43);
    scenecontainer.add(ring44);
    scenecontainer.add(ring45);
    scenecontainer.add(ring46);
    scenecontainer.add(ring47);
    scenecontainer.add(ring48);
    scenecontainer.add(ring49);
    /*ring1.factor = 20;
    ring1.hull = 10000;
    ships.push(ring1);*/
}
var leveloneload = function () {
    setupBullets();
    setupRockets();
    setupColliders();
    starscene = new THREE.Scene();
    starcamera = new THREE.PerspectiveCamera(fov, screenWidth() / screenHeight(), 0.01, 100000);
    addcamera(starcamera);
    skybox(starscene,30);
    var geometry = new THREE.SphereGeometry(6000,128,128);
    var material1 = new THREE.MeshBasicMaterial({color:0xffffff});
    material1.map = THREE.ImageUtils.loadTexture("media/earthmap2.jpg");
    var material = new THREE.MeshLambertMaterial({emissive:0x010101});
    material.map = THREE.ImageUtils.loadTexture("media/earthmap2.jpg");
    var sphere = new THREE.Mesh(geometry,material1);
    starscene.add(sphere);
    atmosphere1 = atmosphere(sphere,100,0xaaaaff,1.1,3,1000,0xaaaaff,0.3,4);
    starscene.add(atmosphere1);
    starrotator = new THREE.Object3D();
    starcamera.position.z = 6200;
    starcamera.position.y = -6200;
    starcamera.rotation.x = Math.PI/2;
    starrotator.add(starcamera);
    starscene.add(starrotator);
    levelonescene = new THREE.Scene();
    levelonecamera = new THREE.PerspectiveCamera(fov, screenWidth() / screenHeight(), 0.01, 100000);
    addcamera(levelonecamera);
    skybox(levelonescene, 30);
    var sphere2 = new THREE.Mesh(geometry,material);
    atmosphere2 = atmosphere(sphere2,100,0xaaaaff,1.1,3,1000,0xaaaaff,0.3,4);
    sphere2.position.x = 10000;
    sphere2.position.z = -10000;
    atmosphere2.position.x = 10000;
    atmosphere2.position.z = -10000;
    var axis = new THREE.Vector3(0,1,0);
    sphere2.rotateOnAxis(axis,5);
    levelonescene.add(sphere2);
    levelonescene.add(atmosphere2);
    geometry = new THREE.SphereGeometry(100, 128, 128);
    material = new THREE.MeshBasicMaterial();
    material.map = THREE.ImageUtils.loadTexture("media/sunmap2.jpg");
    var sphere3 = new THREE.Mesh(geometry, material);
    atmosphere3 = atmosphere(sphere3,10,0xffffff,1.1,3,100,0xffffff,0.3,4);
    sphere3.position.x = 0;
    sphere3.position.y = 0;
    sphere3.position.z = -10000;
    atmosphere3.position.x = 0;
    atmosphere3.position.y = 0;
    atmosphere3.position.z = -10000;
    levelonescene.add(sphere3);
    levelonescene.add(atmosphere3);
    geometry = new THREE.SphereGeometry(300, 128, 128);
    material = new THREE.MeshLambertMaterial();
    material.map = THREE.ImageUtils.loadTexture("media/jupitermap.jpg");
    var sphere4 = new THREE.Mesh(geometry, material);
    sphere4.position.x = 700;
    sphere4.position.y = 0;
    sphere4.position.z = -10000;
    levelonescene.add(sphere4);
    var sunlight = new THREE.PointLight( 0xffffff, 2, 200000, 2 );
    sunlight.position.set( -10000, 0, -10000 );
    levelonescene.add( sunlight );
    //for(var x = -5; x < 5; x++){

    //for(var z = -5; z < 5; z++){
    //levelonescene.add(spheree);
    // }

    // }
    scenecontainer = new THREE.Object3D();
    //var geometry = new THREE.SphereGeometry(4000, 64, 64);
    //var material = new THREE.MeshBasicMaterial();
    //material.map = THREE.ImageUtils.loadTexture("media/jaguar.jpg");
    //var sphere = new THREE.Mesh(geometry, material);
    //scenecontainer.add(sphere);
    musicloop = loopsong("media/overture.ogg");
    musicloop.pause();
    //musicloop.pause();
    //musicloop = playsongcallback("media/introlevel1.ogg",function(){currentpart = 2;});
    //musicloop.pause();
    //vox.volume = 0;
    //vox.pause();
    //vox = playsongcallback("media/introlevel1.ogg",function(){currentpart = 2;});
    createShip();
    testmesh.factor = 10;
    testmesh.hull = 1000000;
    addCollider(testmesh);
    //torus = new THREE.TorusGeometry(10,3,10,10);
    //material = new THREE.MeshBasicMaterial({color:0xaaaaff,transparent:true,opacity:0.5,side:THREE.DoubleSide,wireframe:true});
    //ring = new THREE.Mesh(torus,material);
    //ring.position.z = -3000;
    //scenecontainer.add(ring);
    /*reticle = document.createElement("img");
    reticle.className = "reticle";
    reticle.id = "reticle";
    reticle.src = "media/reticle.svg";
    reticle.alt = "";
    document.body.appendChild(reticle);*/
    currentpart = 0;
    levelonescene.add(scenecontainer);
    createPauseMenu();
    if (pointerlocked == true) {
        leveloneunpause();
    } else {
        levelonepause();
    }
    
}
var leveloneunload = function () {
    musicloop.pause();
    destroyShip();
    destroyPauseMenu();
}
var levelonepause = function () {
    pause();
}
var leveloneunpause = function () {
    unpause();
}
