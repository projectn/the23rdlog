function pointerlock(){
	document.body.requestPointerLock = document.body.requestPointerLock || document.body.mozRequestPointerLock || document.body.webkitRequestPointerLock;
	document.body.requestPointerLock();
    
}
function pointerunlock(){
	document.exitPointerLock = document.exitPointerLock || document.mozExitPointerLock || document.webkitExitPointerLock;
	document.exitPointerLock();
}
pointerlocked = false;
var pointerlockchange = function(event){
	if(document.pointerLockElement === document.body || document.mozPointerLockElement === document.body || document.webkitPointerLockElement === document.body){
		window[levelname+"unpause"]();
		pointerlocked = true;
	}
	else{
		window[levelname+"pause"]();
		pointerlocked = false;
	}
}
var pointerlockerror = function(event){

}
document.addEventListener('pointerlockchange',pointerlockchange,false);
document.addEventListener('mozpointerlockchange',pointerlockchange,false);
document.addEventListener('webkitpointerlockchange',pointerlockchange,false);

document.addEventListener('pointerlockerror',pointerlockerror,false);
document.addEventListener('mozpointerlockerror',pointerlockerror,false);
document.addEventListener('webkitpointerlockerror',pointerlockerror,false);