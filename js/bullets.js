function setupBullets(){
    bullets = [];
    bullet = new THREE.Geometry();
    bullet.vertices.push(new THREE.Vector3(0,0,0),new THREE.Vector3(0,0,-100));
}
function updateBullets(delta){
    for(var i = bullets.length-1; i >= 0; i--){
            bullets[i].time += delta;
            var newv = new THREE.Vector3();
            newv.copy(bullets[i].velocity);
            newv.multiplyScalar(delta);
            bullets[i].position.add(newv);
            news = newv.length();
            newv.multiplyScalar(0.1);
            for(var z = colliders.length-1; z >= 0; z--){
                if(bullets[i].exists == true && bullets[i].owner != colliders[z].owner && bullets[i].getWorldPosition().clone().sub(colliders[z].getWorldPosition()).length() < colliders[z].factor){
                    if(typeof colliders[z].hull !== 'undefined'){
                    console.log(colliders[z].hull);
                    colliders[z].hull -= 10;
                    colliders[z].hit();
                    bullets[i].exists = false;
                    bullets[i].time = 10000;
                    console.log(bullets[i].exists);
                }
                }
                /*if(bullets[i].getWorldPosition().add(newv).add(newv).sub(colliders[z].getWorldPosition()).length() < colliders[z].factor){
                    console.log("HIT");
                }
                if(bullets[i].getWorldPosition().add(newv).add(newv).add(newv).sub(colliders[z].getWorldPosition()).length() < colliders[z].factor){
                    console.log("HIT");
                }
                if(bullets[i].getWorldPosition().add(newv).add(newv).add(newv).add(newv).sub(colliders[z].getWorldPosition()).length() < colliders[z].factor){
                    console.log("HIT");
                }
                if(bullets[i].getWorldPosition().add(newv).add(newv).add(newv).add(newv).add(newv).sub(colliders[z].getWorldPosition()).length() < colliders[z].factor){
                    console.log("HIT");
                }
                if(bullets[i].getWorldPosition().add(newv).add(newv).add(newv).add(newv).add(newv).add(newv).sub(colliders[z].getWorldPosition()).length() < colliders[z].factor){
                    console.log("HIT");
                }
                if(bullets[i].getWorldPosition().add(newv).add(newv).add(newv).add(newv).add(newv).add(newv).add(newv).sub(colliders[z].getWorldPosition()).length() < colliders[z].factor){
                    console.log("HIT");
                }
                if(bullets[i].getWorldPosition().add(newv).add(newv).add(newv).add(newv).add(newv).add(newv).add(newv).add(newv).sub(colliders[z].getWorldPosition()).length() < colliders[z].factor){
                    console.log("HIT");
                }
                if(bullets[i].getWorldPosition().add(newv).add(newv).add(newv).add(newv).add(newv).add(newv).add(newv).add(newv).add(newv).sub(colliders[z].getWorldPosition()).length() < colliders[z].factor){
                    console.log("HIT");
                }
                if(bullets[i].getWorldPosition().add(newv).add(newv).add(newv).add(newv).add(newv).add(newv).add(newv).add(newv).add(newv).add(newv).sub(colliders[z].getWorldPosition()).length() < colliders[z].factor){
                    console.log("HIT");
                }
                if(bullets[i].getWorldPosition().add(newv).add(newv).add(newv).add(newv).add(newv).add(newv).add(newv).add(newv).add(newv).add(newv).add(newv).sub(colliders[z].getWorldPosition()).length() < news/10){
                    console.log("HIT");
                }*/
            }
            if(bullets[i].time > 5 || bullets[i].exists == false){
                //bulletName = scenecontainer.getObjectByName(bullets[i].name);
                scenecontainer.remove(bullets[i]);
                bullets.splice(i,1);
            }
        }
}
function shoot(mesh,v,owner,width,speed){
    var testbullet = new THREE.Line(bullet, new THREE.LineBasicMaterial({color:0xFFC400,linewidth:1}));
        
    //var testbullet = new THREE.Mesh(new THREE.SphereGeometry(1,32,32),new THREE.MeshNormalMaterial());
    testbullet.position.copy(mesh.localToWorld(new THREE.Vector3(-width,0,0)));
    testbullet.time = 0;
    testbullet.name = bullets.length.toString();
    testbullet.velocity = mesh.localToWorld(new THREE.Vector3(0,0,-speed));
    testbullet.velocity.sub(mesh.getWorldPosition());
    var newz = new THREE.Vector3();
    newz.copy(v);
    newz.add(mesh.getWorldPosition());
    mesh.worldToLocal(newz);
    //newz.x = 0;
    //newz.y = 0;
    mesh.localToWorld(newz);
    newz.sub(mesh.getWorldPosition());
    testbullet.velocity.add(newz);
    testbullet.position.sub(scenecontainer.position);
    testbullet.rotation.copy(mesh.rotation);
    testbullet.owner = owner;
    testbullet.exists = true;
    bullets.push(testbullet);
    scenecontainer.add(testbullet);
    var testbullet2 = new THREE.Line(bullet, new THREE.LineBasicMaterial({color:0xFFC400,linewidth:1}));
    testbullet2.position.copy(mesh.localToWorld(new THREE.Vector3(width,0,0)));
    testbullet2.time = 0;
    testbullet2.name = bullets.length.toString();
    testbullet2.velocity = mesh.localToWorld(new THREE.Vector3(0,0,-speed));
    testbullet2.velocity.sub(mesh.getWorldPosition());
    testbullet2.velocity.add(newz);
    testbullet2.position.sub(scenecontainer.position);
    testbullet2.rotation.copy(mesh.rotation);
    testbullet2.owner = owner;
    testbullet2.exists = true;
    bullets.push(testbullet2);
    scenecontainer.add(testbullet2);
}