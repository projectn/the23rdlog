function setupRockets(){
	rockets = [];
	rocket = new THREE.ConeGeometry(5,20,32);
}
function updateRockets(delta){
    for(var i = rockets.length-1; i >= 0; i--){
            rockets[i].time += delta;
            var newv = new THREE.Vector3();
            newv.copy(rockets[i].velocity);
            newv.multiplyScalar(delta);
            rockets[i].position.add(newv);
            news = newv.length();
            newv.multiplyScalar(0.1);
            if(rockets[i].target != 0){
            rockets[i].velocity.add(rockets[i].target.getWorldPosition().clone().sub(rockets[i].getWorldPosition()).normalize().multiplyScalar(50));
            rockets[i].velocity.divideScalar(1.1);
        	}
        	/*var newDir = new THREE.Vector3(1, 1, 1);
			var pos = new THREE.Vector3();
			pos.copy(rockets[i].velocity);
			pos.addVectors(newDir, rockets[i].position);
			rockets[i].lookAt(pos);
			rockets[i].rotateY(-Math.PI/2);*/
            for(var z = colliders.length-1; z >= 0; z--){
                if(rockets[i].exists == true && rockets[i].getWorldPosition().sub(colliders[z].getWorldPosition()).length() < colliders[z].factor){
                    //console.log(colliders[z].hull);
                    if(typeof colliders[z].hull !== 'undefined'){
                    colliders[z].hull -= 10;
                    colliders[z].hit();
                    rockets[i].exists = false;
                    rockets[i].time = 10000;
                    console.log(rockets[i].exists);
                    }
                }
                /*if(rockets[i].getWorldPosition().add(newv).add(newv).sub(colliders[z].getWorldPosition()).length() < colliders[z].factor){
                    console.log("HIT");
                }
                if(rockets[i].getWorldPosition().add(newv).add(newv).add(newv).sub(colliders[z].getWorldPosition()).length() < colliders[z].factor){
                    console.log("HIT");
                }
                if(rockets[i].getWorldPosition().add(newv).add(newv).add(newv).add(newv).sub(colliders[z].getWorldPosition()).length() < colliders[z].factor){
                    console.log("HIT");
                }
                if(rockets[i].getWorldPosition().add(newv).add(newv).add(newv).add(newv).add(newv).sub(colliders[z].getWorldPosition()).length() < colliders[z].factor){
                    console.log("HIT");
                }
                if(rockets[i].getWorldPosition().add(newv).add(newv).add(newv).add(newv).add(newv).add(newv).sub(colliders[z].getWorldPosition()).length() < colliders[z].factor){
                    console.log("HIT");
                }
                if(rockets[i].getWorldPosition().add(newv).add(newv).add(newv).add(newv).add(newv).add(newv).add(newv).sub(colliders[z].getWorldPosition()).length() < colliders[z].factor){
                    console.log("HIT");
                }
                if(rockets[i].getWorldPosition().add(newv).add(newv).add(newv).add(newv).add(newv).add(newv).add(newv).add(newv).sub(colliders[z].getWorldPosition()).length() < colliders[z].factor){
                    console.log("HIT");
                }
                if(rockets[i].getWorldPosition().add(newv).add(newv).add(newv).add(newv).add(newv).add(newv).add(newv).add(newv).add(newv).sub(colliders[z].getWorldPosition()).length() < colliders[z].factor){
                    console.log("HIT");
                }
                if(rockets[i].getWorldPosition().add(newv).add(newv).add(newv).add(newv).add(newv).add(newv).add(newv).add(newv).add(newv).add(newv).sub(colliders[z].getWorldPosition()).length() < colliders[z].factor){
                    console.log("HIT");
                }
                if(rockets[i].getWorldPosition().add(newv).add(newv).add(newv).add(newv).add(newv).add(newv).add(newv).add(newv).add(newv).add(newv).add(newv).sub(colliders[z].getWorldPosition()).length() < news/10){
                    console.log("HIT");
                }*/
            }
            if(rockets[i].time > 5 || rockets[i].exists == false){
                //rocketName = scenecontainer.getObjectByName(rockets[i].name);
                scenecontainer.remove(rockets[i]);
                rockets.splice(i,1);
            }
        }
}
function shootRocket(mesh,v,owner,target){
    var testrocket = new THREE.Mesh(rocket, new THREE.MeshBasicMaterial({color:0xFFC400}));
        
    //var testrocket = new THREE.Mesh(new THREE.SphereGeometry(1,32,32),new THREE.MeshNormalMaterial());
    testrocket.position.copy(mesh.localToWorld(new THREE.Vector3(0,-5,0)));
    testrocket.time = 0;
    testrocket.name = rockets.length.toString();
    testrocket.velocity = mesh.localToWorld(new THREE.Vector3(0,0,-500));
    testrocket.velocity.sub(mesh.getWorldPosition());
    testrocket.up = new THREE.Vector3(0,0,-1);
    var newz = new THREE.Vector3();
    newz.copy(v);
    newz.add(mesh.getWorldPosition());
    mesh.worldToLocal(newz);
    //newz.x = 0;
    //newz.y = 0;
    mesh.localToWorld(newz);
    newz.sub(mesh.getWorldPosition());
    testrocket.velocity.add(newz);
    testrocket.position.sub(scenecontainer.position);
    testrocket.rotation.copy(mesh.rotation);
    testrocket.target = target;
    testrocket.owner = owner;
    testrocket.exists = true;
    rockets.push(testrocket);
    scenecontainer.add(testrocket);
}