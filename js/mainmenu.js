function exit() {
    exitinterval = setInterval(exitloop,20);
}
function exitloop(){
    fade = document.getElementById("fade");
    if(parseFloat(fade.style.opacity) < 1 && musicloop.volume > 0){
    musicloop.volume -= 0.03999;
    fade.style.opacity=parseFloat(fade.style.opacity)+0.04;
    }
    else{
        fade.style.opacity = 1;
        musicloop.pause();
        musicloop.volume = 0;
        clearInterval(exitinterval);
        exitDone();
    }
}
function exitDone(){
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", "http://localhost:8000", true); // false for synchronous request
    xmlHttp.send(null);
}
var mainmenu = function (delta) {
    mainmenucamera.rotation.x -= 0.1 * delta;
    mainmenucamera.rotation.y -= 0.05 * delta;
    renderer.render(mainmenuscene, mainmenucamera);
}
var mainmenuload = function () {
    mainmenuscene = new THREE.Scene();
    mainmenucamera = new THREE.PerspectiveCamera(fov, screenWidth() / screenHeight(), 0.1, 100000);
    addcamera(mainmenucamera);
    skybox(mainmenuscene, 30);
    musicloop = loopsong("media/bensound-slowmotion.ogg");
    musicloop.volume = 0.5;
    title = document.createElement("div");
    title.className = "menu";
    title.id = "mainmenutitle";
    title.innerHTML = "The 23rd Log<br /><br /><a onclick='pointerlock();switchLevel(\"levelone\");'>Play</a><br /><a onclick='exit()'>Exit</a>"; //TODO: Request pointer lock when PLAY button is clicked. Add JS helper file for pointer locks. https://developer.mozilla.org/en-US/docs/Web/API/Pointer_Lock_API
    document.body.appendChild(title);
}
var mainmenuunload = function () {
    document.getElementById("mainmenutitle").parentNode.removeChild(document.getElementById("mainmenutitle"));
}
var mainmenupause = function () {
    console.log("hi");
}
var mainmenuunpause = function () {
    console.log("hi")
}