var levelthree = function (delta) {
    if (scenecontainer.position.x > limit) {
        scenecontainer.position.x = limit;
    } else if (scenecontainer.position.x < (limit * -1)) {
        scenecontainer.position.x = (limit * -1);
    }
    if (scenecontainer.position.y > limit) {
        scenecontainer.position.y = limit;
    } else if (scenecontainer.position.y < (limit * -1)) {
        scenecontainer.position.y = (limit * -1);
    }
    if (scenecontainer.position.z > limit) {
        scenecontainer.position.z = limit;
    } else if (scenecontainer.position.z < (limit * -1)) {
        scenecontainer.position.z = (limit * -1);
    }
    if (sound) {
        musicloop.volume = 0.5;
    } else {
        musicloop.volume = 0;
    }
    if (pointerlocked == true) {
        if(currentpart == 1){
        }
        if(currentpart == 2){
            //check for power2 destroyed.
        }
        if(currentpart == 3){
            //check for power3 destroyed.
            //currentpart = 4;
        }
        if(currentpart == 4){
            vox = playsongcallback("media/ending.ogg",function(){switchLevel("mainmenu");pointerunlock();});
            currentpart = 5;
        }
        if(currentpart == 0){
            vox = playsong("media/peptalk.ogg");
            //musicloop = loopsong("media/overture.ogg");
            currentpart = 1;
        }
        if(typeof cone != undefined){
        updateConeThree();
    }   
        updateColliders();
        updateControls(delta);
        updateShip(delta);
        updateBullets(delta);
        renderer.render(levelthreescene,levelthreecamera);
    }
}
var motherShip = function(){
    enemy = new THREE.Mesh(enemygeometry,enemymaterial.clone());
    enemy.scale.set(3000,3000,3000);
    enemy.position.x = 10000;
    enemy.position.z = -10000;
    scenecontainer.add(enemy);
}
var powerConduits = function(){
    power1 = new THREE.Mesh(new THREE.BoxGeometry(100,100,100),new THREE.MeshStandardMaterial({
        color: 0xaaaaaa,
        emmisive: 0x444444,
        shading: THREE.SmoothShading,
        metalness: 1
    }));
    power1.position.x = 10000;
    power1.position.z = -10000;
    power1.rotateX(Math.random()*5000-2500);
    power1.rotateY(Math.random()*5000-2500);
    power1.rotateZ(Math.random()*5000-2500);
    power1.translateZ(-2500);
    scenecontainer.add(power1);
    power2 = new THREE.Mesh(new THREE.BoxGeometry(100,100,100),new THREE.MeshStandardMaterial({
        color: 0xaaaaaa,
        emmisive: 0x444444,
        shading: THREE.SmoothShading,
        metalness: 1
    }));
    power2.position.x = 10000;
    power2.position.z = -10000;
    power2.rotateX(Math.random()*5000-2500);
    power2.rotateY(Math.random()*5000-2500);
    power2.rotateZ(Math.random()*5000-2500);
    power2.translateZ(-2500);
    scenecontainer.add(power2);
    power3 = new THREE.Mesh(new THREE.BoxGeometry(100,100,100),new THREE.MeshStandardMaterial({
        color: 0xaaaaaa,
        emmisive: 0x444444,
        shading: THREE.SmoothShading,
        metalness: 1
    }));
    power3.position.x = 10000;
    power3.position.z = -10000;
    power3.rotateX(Math.random()*5000-2500);
    power3.rotateY(Math.random()*5000-2500);
    power3.rotateZ(Math.random()*5000-2500);
    power3.translateZ(-2500);
    scenecontainer.add(power3);
    powerr = power1;
    power1.hull = 500;
    power1.factor = 100;
    //enemy1.destroyee = false;
    power1.hit = function(){

    }
    power1.destroy = function(){
        currentpart = 2;
        scenecontainer.remove(power1);
    }
    power2.hull = 500;
    power2.factor = 100;
    //enemy1.destroyee = false;
    power2.hit = function(){

    }
    power2.destroy = function(){
        currentpart = 3;
        scenecontainer.remove(power2);
    }
    power3.hull = 500;
    power3.factor = 100;
    //enemy1.destroyee = false;
    power3.hit = function(){

    }
    power3.destroy = function(){
        currentpart = 4;
        scenecontainer.remove(power3);
    }
    addCollider(power1);
    addCollider(power2);
    addCollider(power3);
}
var levelthreeload = function() {
        currentpart = 0;
        musicloop = loopsong("media/Five Armies.ogg");
        musicloop.volume = 0.5;
    musicloop.pause();
    enemygeometry = new THREE.BoxGeometry(100,100,100);
    enemymaterial = new THREE.MeshNormalMaterial({});
    var loader = new THREE.JSONLoader();
    loader.load('media/enemy.full.json', function(geometry,materials) {
        enemymaterial = new THREE.MultiMaterial( materials );
        enemygeometry = geometry;
        motherShip();
        powerConduits();
    });
    enemylowgeometry = new THREE.BoxGeometry(100,100,100);
    enemylowmaterial = new THREE.MeshNormalMaterial({});
    loader.load('media/enemy.low.json', function(geometry,materials) {
        enemylowmaterial = new THREE.MultiMaterial( materials );
        enemylowgeometry = geometry;
    });
    var sunlight = new THREE.PointLight( 0xffffff, 2, 200000, 2 );
    sunlight.position.set( -10000, 10000, -10000 );
    
        setupBullets();
    setupRockets();
    setupColliders();
    levelthreescene = new THREE.Scene();
    levelthreecamera = new THREE.PerspectiveCamera(fov, screenWidth() / screenHeight(), 0.01, 100000);
    addcamera(levelthreecamera);
    levelthreescene.add( sunlight );
    skybox(levelthreescene, 30);
    createShip();
            scenecontainer = new THREE.Object3D();
levelthreescene.add(scenecontainer);
    //motherShip();
    var conegeometry = new THREE.ConeGeometry(0.005,0.02,3);
    conegeometry.rotateX(Math.PI/2);
    var conematerial = new THREE.MeshStandardMaterial({color: 0x00aaff});
    cone = new THREE.Mesh(conegeometry,conematerial);
    cone.position.z = -0.1;
    cone.position.y = -0.02;
    levelthreescene.add(cone);
    conedummy = new THREE.Object3D();
    conedummy.position.z = -0.1;
    conedummy.position.y = -0.02;
    testmesh.add(conedummy);
        createPauseMenu();
    if (pointerlocked == true) {
        levelthreeunpause();
    } else {
        levelthreepause();
    }
    }
    var levelthreeunload = function () {
        
    musicloop.pause();
    destroyShip();
    destroyPauseMenu();
}
function updateConeThree(){
    if(currentpart == 2){
        var myDirectionVector = power2.getWorldPosition().clone();
    }
    else if(currentpart == 3){
        var myDirectionVector = power3.getWorldPosition().clone();
    }
    else{
        var myDirectionVector = power1.getWorldPosition().clone();
    }
    var mx = new THREE.Matrix4().lookAt(myDirectionVector,new THREE.Vector3(0,0,0),new THREE.Vector3(0,1,0));
    var qt = new THREE.Quaternion().setFromRotationMatrix(mx);
    //mesh.rotation.x += 0.01;
    //mesh.rotation.y += 0.02;
    
    
    cone.quaternion.copy( qt );
    cone.position.setFromMatrixPosition(conedummy.matrixWorld);
    //cone.lookAt(center.getWorldPosition());
}
var levelthreepause = function () {
    pause();
}
var levelthreeunpause = function () {
    unpause();
}
