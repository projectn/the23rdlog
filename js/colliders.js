function setupColliders(){
	colliders = [];
}
function addCollider(z){
	colliders.push(z);
	var sphere = new THREE.SphereGeometry(z.factor*1.5,4,2);
	var material = new THREE.MeshNormalMaterial({wireframe:true});
	var mesh = new THREE.Mesh(sphere,material);
	mesh.visible = false;
	z.target = mesh;
	z.add(mesh);
}
function updateColliders(){
	for(var z = colliders.length-1; z >= 0; z--){
		if(typeof colliders[z].hull !== 'undefined'){
		if(colliders[z].hull <= 0){
            for(var i = rockets.length-1; i>=0; i--){
            	if(rockets[i].target == colliders[z]){
            		rockets[i].target = 0;
            	}
            }
            scenecontainer.remove(colliders[z]);
            colliders[z].destroy();
            colliders.splice(z,1);
		}
	}
	}
}
function checkCollider(e){
	for(var z = colliders.length-1; z >= 0; z--){
		vector = e.getWorldPosition().clone().sub(colliders[z].getWorldPosition());
		if(vector.x*vector.x + vector.y*vector.y + vector.z*vector.z  < (e.factor+colliders[z].factor)*(e.factor+colliders[z].factor) && vector.length()!= 0){
			e.lastKnownPosition();
			//COMMENTING OUT FOR THE SAKE OF SIMPLICITY
			/*console.log("hi");
			vector2 = vector.clone();
			vector2.normalize();
			vector2.multiplyScalar((e.factor+colliders[z].factor));
			vector2.add(colliders[z].getWorldPosition());
			e.worldToLocal(vector2);
			e.position.copy(vector2);*/
		}
	}
}